#define GLEW_STATIC
#define OVER_POWER

#define BUFFER_OFFSET(bytes) ((GLubyte*) NULL + (bytes))
#define WINDOW_INITIAL_WIDTH	1024
#define WINDOW_INITIAL_HEIGHT	733
#define RESOLUTION_DIVIDER		1

#ifdef OVER_POWER
	#define MAX_PARTICLES			250000
	#define PARTICLES_RELEASE_DELAY	0.075f
	#define BIG_BOX_POSITION		PxVec3(-20.4436f, 1.466583f, -25.9824f)
	#define BIG_BOX_SIZE			PxVec3(0.875f, 1.75f, 0.875f)
	#define PARTICLES_DISTANCE		0.6
#endif

#ifdef HIGH_POWER_30FPS
	#define MAX_PARTICLES			60000
	#define PARTICLES_RELEASE_DELAY	0.075f
	#define BIG_BOX_POSITION		PxVec3(-20.4436f, 1.466583f, -25.9824f)
	#define BIG_BOX_SIZE			PxVec3(0.875f, 1.75f, 0.875f)
	#define PARTICLES_DISTANCE		0.6
#endif

#ifdef HIGH_POWER_60FPS
	#define MAX_PARTICLES			37500
	#define PARTICLES_RELEASE_DELAY	0.075f
	#define BIG_BOX_POSITION		PxVec3(-20.4436f, 1.466583f, -25.9824f)
	#define BIG_BOX_SIZE			PxVec3(0.875f, 1.75f, 0.875f)
	#define PARTICLES_DISTANCE		0.6
#endif

#ifdef LOW_POWER_60FPS
	#define MAX_PARTICLES			7000
	#define PARTICLES_RELEASE_DELAY	0.75f
	#define BIG_BOX_POSITION		PxVec3(-20.4436f, 1.466583f, -25.9824f)
	#define BIG_BOX_SIZE			PxVec3(0.875f, 1.75f, 0.875f)
	#define PARTICLES_DISTANCE		0.6
#endif

#define MAX_BILATERAL_FILTER_KERNEL_SIZE	50

#define CURVATURE_FLOW				0
#define FULLKERNEL_BILATERAL_FILTER	1
#define SEPARATED_BILATERAL_FILTER	2
#define NO_SMOOTHING				3

#define NONPHOTOREALISTIC_RENDERING	0
#define PHOTOREALISTIC_RENDERING	1

#define DENSITY_FOAMANDDROPLET		0
#define WEBERNUMBER_FOAMANDDROPLET	1

#define RENDER_BACKGROUND_ONLY				0
#define RENDER_PARTICLEDEPTH_ONLY			1
#define RENDER_PARTICLESMOOTHEDDEPTH_ONLY	2
#define RENDER_PARTICLEFOAM_ONLY			3
#define RENDER_PARTICLETHICKNESS_ONLY		4
#define RENDER_EVERYTHING					5
#define RENDER_PARTICLE_CLOUD				6

#define NO_OPTICAL_EFFECTS	0
#define ONLY_REFRACTION		1
#define ONLY_REFLECTION		2
#define ALL_OPTICAL_EFFECTS	3

#include <iostream>

#include <GL\glew.h>
#include <glm\glm.hpp>
#include <glm\gtc\matrix_transform.hpp>
#include <glm\gtc\type_ptr.hpp>
#include <glm\gtx\fast_square_root.hpp>
#include <glm\gtc\quaternion.hpp>
#include <GLFW\glfw3.h>
#include <PhysX\PxPhysicsAPI.h>
#include <FreeImage.h>

#include "PhysicsSimulator.h"

using namespace std;

using namespace glm;
using namespace physx;

GLFWwindow* window;
int width, height;
int midWindowX = WINDOW_INITIAL_WIDTH/2, midWindowY  = WINDOW_INITIAL_HEIGHT/2;

float horizontalAngle = -3.94197f;
float verticalAngle = -0.215594f;

vec3 eye = vec3(-50.0551f, 14.1472f, 2.28678f);
vec3 center, rightVector, upVector;
vec4 lightPosition = vec4(120.0f, 50.0f, 110.0f, 0.0f);
vec3 lightIntensity = vec3(1.0f, 1.0f, 1.0f);

float fovy = 45.0f;
float near = 0.1f;
float far = 1000.0f;

float speed = 5.0f;
float mouseSpeed = 0.005f;
float deltaTime = 0.0f;

float movementSpeedFactor = 1.0f;

// Skybox
static const GLfloat skyboxVertices[] = {
    // Positions          
    -1.0f,  1.0f, -1.0f,
    -1.0f, -1.0f, -1.0f,
     1.0f, -1.0f, -1.0f,
     1.0f, -1.0f, -1.0f,
     1.0f,  1.0f, -1.0f,
    -1.0f,  1.0f, -1.0f,
  
    -1.0f, -1.0f,  1.0f,
    -1.0f, -1.0f, -1.0f,
    -1.0f,  1.0f, -1.0f,
    -1.0f,  1.0f, -1.0f,
    -1.0f,  1.0f,  1.0f,
    -1.0f, -1.0f,  1.0f,
  
     1.0f, -1.0f, -1.0f,
     1.0f, -1.0f,  1.0f,
     1.0f,  1.0f,  1.0f,
     1.0f,  1.0f,  1.0f,
     1.0f,  1.0f, -1.0f,
     1.0f, -1.0f, -1.0f,
   
    -1.0f, -1.0f,  1.0f,
    -1.0f,  1.0f,  1.0f,
     1.0f,  1.0f,  1.0f,
     1.0f,  1.0f,  1.0f,
     1.0f, -1.0f,  1.0f,
    -1.0f, -1.0f,  1.0f,
  
    -1.0f,  1.0f, -1.0f,
     1.0f,  1.0f, -1.0f,
     1.0f,  1.0f,  1.0f,
     1.0f,  1.0f,  1.0f,
    -1.0f,  1.0f,  1.0f,
    -1.0f,  1.0f, -1.0f,
  
    -1.0f, -1.0f, -1.0f,
    -1.0f, -1.0f,  1.0f,
     1.0f, -1.0f, -1.0f,
     1.0f, -1.0f, -1.0f,
    -1.0f, -1.0f,  1.0f,
     1.0f, -1.0f,  1.0f
};

// Water rendering
static const GLfloat screenAlignedQuad[] = {
	-1.0f, -1.0f, 0.0f,
	 1.0f, -1.0f, 0.0f,
	-1.0f,  1.0f, 0.0f,
	-1.0f,  1.0f, 0.0f,
	 1.0f, -1.0f, 0.0f,
	 1.0f,  1.0f, 0.0f
};

// TODO: hoje as variaveis de limiares sao sobre a densidade das particulas. Rever os valores quando mudar pro Weber Number
float foamThreshold = 2.0f;

// Rendering methods
int smoothingMethod = SEPARATED_BILATERAL_FILTER;
int foamDropletGenerationMethod = DENSITY_FOAMANDDROPLET;
int whatToRender = RENDER_EVERYTHING;
int opticalEffectsToRender = ALL_OPTICAL_EFFECTS;
bool renderingStyle = NONPHOTOREALISTIC_RENDERING;

// Curvature flow
int curvatureFlowNumberOfIterations = 220;
float curvatureFlowEpsilon = 0.00002;

// Bilateral Filter
int separatedIterativeBilateralFilterNumberOfIterations = 3;
int bilateralFilterKernelSize = 30;
float bilateralFilter1DKernel[MAX_BILATERAL_FILTER_KERNEL_SIZE];
float bilateralFilterDomainSigma = 0.01;
float bilateralFilterSpatialSigma = 10.00;

// Framebuffers and textures
GLuint backgroundFrameBuffer, backgroundTexture, backgroundDepthTexture;
GLuint depthFrameBuffer, depthTexture;
GLuint smoothedDepthFrameBuffer[2], smoothedDepthTexture[2];
GLuint foamDepthFrameBuffer, foamDepthTexture;
GLuint depthStencilFrameBuffer, depthStencilTexture;
GLuint foamDepthStencilFrameBuffer, foamDepthStencilTexture;
GLuint thicknessFrameBuffer, thicknessTexture;

// Shader reading
ShaderReader *shaderReader;

// PhysX
PhysicsSimulator *physicsSimulator;
RenderActor boxStructure;
PxRigidDynamic* lastAddedBox;
RenderActor bigBoxStructure;
PxRigidDynamic* bigBox;

static void error_callback(int error, const char* description)
{
	fputs(description, stderr);
}

static void FreeImageErrorHandler(FREE_IMAGE_FORMAT fif, const char *message)
{
	printf("\n*** ");
	if (fif != FIF_UNKNOWN)
		printf("%s Format\n", FreeImage_GetFormatFromFIF(fif));

	printf(message);
	printf(" ***\n");
}

static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode)
{
	if(action == GLFW_PRESS)
	{
		if (key == GLFW_KEY_0) 
		{	
			smoothingMethod = SEPARATED_BILATERAL_FILTER;
			cout << "Smoothing method: Separated Bilateral Filter" << endl;
		}

		if (key == GLFW_KEY_1) 
		{	
			smoothingMethod = FULLKERNEL_BILATERAL_FILTER;
			cout << "Smoothing method: Full Kernel Bilateral Filter" << endl;
		}

		if (key == GLFW_KEY_2) 
		{	
			smoothingMethod = CURVATURE_FLOW;
			cout << "Smoothing method: Curvature Flow" << endl;
		}

		if (key == GLFW_KEY_3 && foamDropletGenerationMethod != WEBERNUMBER_FOAMANDDROPLET) 
		{	
			foamDropletGenerationMethod = WEBERNUMBER_FOAMANDDROPLET;
			foamThreshold *= 0.001;
			cout << "Foam and droplet generation method: by Weber Number" << endl;
		}

		if (key == GLFW_KEY_4 && foamDropletGenerationMethod != DENSITY_FOAMANDDROPLET) 
		{	
			foamDropletGenerationMethod = DENSITY_FOAMANDDROPLET;
			foamThreshold *= 1000;
			cout << "Foam and droplet generation method: by Density" << endl;
		}

		if (key == GLFW_KEY_5) 
		{	
			renderingStyle = NONPHOTOREALISTIC_RENDERING;
			cout << "Rendering style: Non-photorealistic" << endl;
		}

		if (key == GLFW_KEY_6) 
		{	
			renderingStyle = PHOTOREALISTIC_RENDERING;
			cout << "Rendering style: Photorealistic" << endl;
		}

		if (key == GLFW_KEY_8) 
		{	
			smoothingMethod = NO_SMOOTHING;
			cout << "Smoothing method: No Smoothing" << endl;
		}
		
		if (key == GLFW_KEY_P) physicsSimulator->getInstance().togglePhysicsSimulationRunningState();
		if (key == GLFW_KEY_ESCAPE) glfwSetWindowShouldClose(window, GL_TRUE);	
	}
	else
	{
		if (key == GLFW_KEY_H || key == GLFW_KEY_J)
		{
			if (foamDropletGenerationMethod == WEBERNUMBER_FOAMANDDROPLET)
			{
				foamThreshold += (key == GLFW_KEY_H) ? -0.0001 : +0.0001;
			}
			else if (foamDropletGenerationMethod == DENSITY_FOAMANDDROPLET)
			{
				foamThreshold += (key == GLFW_KEY_H) ? -0.1 : +0.1;
			}
			if (foamThreshold < 0.0f) foamThreshold = 0.0f;
			cout << "Foam threshold: " << foamThreshold << endl;
		}

		if (key == GLFW_KEY_Z || key == GLFW_KEY_X)
		{
			if (smoothingMethod == SEPARATED_BILATERAL_FILTER || smoothingMethod == FULLKERNEL_BILATERAL_FILTER)
			{
				separatedIterativeBilateralFilterNumberOfIterations += (key == GLFW_KEY_Z) ? +1 : -1;
				if (separatedIterativeBilateralFilterNumberOfIterations < 0) separatedIterativeBilateralFilterNumberOfIterations = 0;
				cout << "Bilateral filter Iterations: " << separatedIterativeBilateralFilterNumberOfIterations << endl;
			}

			if (smoothingMethod == CURVATURE_FLOW)
			{
				curvatureFlowNumberOfIterations += (key == GLFW_KEY_Z) ? +1 : -1;
				if (curvatureFlowNumberOfIterations < 0) curvatureFlowNumberOfIterations = 0;
				cout << "Curvature Flow Iterations: " << curvatureFlowNumberOfIterations << endl;
			}
		}

		if (key == GLFW_KEY_C || key == GLFW_KEY_V)
		{
			if (smoothingMethod == SEPARATED_BILATERAL_FILTER || smoothingMethod == FULLKERNEL_BILATERAL_FILTER)
			{
				bilateralFilterKernelSize += (key == GLFW_KEY_C) ? +1 : -1;
				if (bilateralFilterKernelSize < 0) bilateralFilterKernelSize = 0;
				if (bilateralFilterKernelSize > MAX_BILATERAL_FILTER_KERNEL_SIZE) bilateralFilterKernelSize = MAX_BILATERAL_FILTER_KERNEL_SIZE;
				cout << "Bilateral filter Kernel size: " << bilateralFilterKernelSize << endl;
			}

			if (smoothingMethod == CURVATURE_FLOW)
			{
				curvatureFlowEpsilon += (key == GLFW_KEY_C) ? +0.00001 : -0.00001;
				if (curvatureFlowEpsilon < 0.0f) bilateralFilterKernelSize = 0.0f;
				cout << "Curvature Flow Epsilon: " << curvatureFlowEpsilon << endl;
			}
		}		
		
		if (key == GLFW_KEY_T || key == GLFW_KEY_Y)
		{
			if (smoothingMethod == SEPARATED_BILATERAL_FILTER || smoothingMethod == FULLKERNEL_BILATERAL_FILTER)
			{
				bilateralFilterDomainSigma  += (key == GLFW_KEY_T) ? +0.01 : -0.01;
				if (bilateralFilterDomainSigma < 0.0f) bilateralFilterDomainSigma = 0.0f;
				cout << "Bilateral filter Domain sigma: " << bilateralFilterDomainSigma << endl;
			}
		}		

		if (key == GLFW_KEY_U || key == GLFW_KEY_I)
		{
			if (smoothingMethod == SEPARATED_BILATERAL_FILTER || smoothingMethod == FULLKERNEL_BILATERAL_FILTER)
			{
				bilateralFilterSpatialSigma  += (key == GLFW_KEY_U) ? +0.01 : -0.01;
				if (bilateralFilterSpatialSigma < 0.0f) bilateralFilterSpatialSigma = 0.0f;
				cout << "Bilateral filter Spatial sigma: " << bilateralFilterSpatialSigma << endl;
			}
		}

		if (key == GLFW_KEY_7)
		{
			opticalEffectsToRender = opticalEffectsToRender == 3 ? 0 : opticalEffectsToRender + 1;

			switch(whatToRender)
			{
				case NO_OPTICAL_EFFECTS:
					cout << "Rendering without optical effects." << endl;
					break;
				case ONLY_REFRACTION:
					cout << "Rendering with only refraction." << endl;
					break;
				case ONLY_REFLECTION:
					cout << "Rendering with only reflection." << endl;
					break;
				case ALL_OPTICAL_EFFECTS:
					cout << "Rendering with all optical effects." << endl;
					break;
				default:
					break;
			}
		}

		if (key == GLFW_KEY_9)
		{
			whatToRender = whatToRender == 6 ? 0 : whatToRender + 1;

			switch(whatToRender)
			{
				case RENDER_BACKGROUND_ONLY:
					cout << "Rendering only background." << endl;
					break;
				case RENDER_PARTICLEDEPTH_ONLY:
					cout << "Rendering only particle depth." << endl;
					break;
				case RENDER_PARTICLEFOAM_ONLY:
					cout << "Rendering only particle foam depth." << endl;
					break;
				case RENDER_PARTICLETHICKNESS_ONLY:
					cout << "Rendering only particle thickness." << endl;
					break;
				case RENDER_PARTICLESMOOTHEDDEPTH_ONLY:
					cout << "Rendering only particle smoothed depth." << endl;
					break;
				case RENDER_EVERYTHING:
					cout << "Rendering everything." << endl;
					break;
				case RENDER_PARTICLE_CLOUD:
					cout << "Rendering only the particle cloud." << endl;
					break;
				default:
					break;
			}
		}

		if (key == GLFW_KEY_W) eye += center * deltaTime * speed;
		if (key == GLFW_KEY_A) eye -= rightVector * deltaTime * speed;
		if (key == GLFW_KEY_S) eye -= center * deltaTime * speed;
		if (key == GLFW_KEY_D) eye += rightVector * deltaTime * speed;
		
		if (key == GLFW_KEY_UP && lastAddedBox) PxRigidBodyExt::addForceAtPos(*lastAddedBox, PxVec3(center.x, center.y, center.z * 20.0f) * speed, PxVec3(0.0f, 0.0f, 0.0f));
		if (key == GLFW_KEY_LEFT && lastAddedBox) PxRigidBodyExt::addForceAtPos(*lastAddedBox, PxVec3(center.x * -20.0f, center.y, center.z) * speed, PxVec3(0.0f, 0.0f, 0.0f));
		if (key == GLFW_KEY_DOWN && lastAddedBox) PxRigidBodyExt::addForceAtPos(*lastAddedBox, PxVec3(center.x, center.y, center.z * -20.0f) * speed, PxVec3(0.0f, 0.0f, 0.0f));
		if (key == GLFW_KEY_RIGHT && lastAddedBox) PxRigidBodyExt::addForceAtPos(*lastAddedBox, PxVec3(center.x * 20.0f, center.y, center.z) * speed, PxVec3(0.0f, 0.0f, 0.0f));  
	}
}

static void mousebutton_callback(GLFWwindow* window, int button, int action, int mods)
{
	if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS)
	{
		vec3 boxPosition = eye + center * 50.0f;
		PxVec3 newBoxPosition = PxVec3(boxPosition.x, boxPosition.y, boxPosition.z);
		boxStructure = physicsSimulator->getInstance().addBox(newBoxPosition, PxBoxGeometry(PxVec3(0.5f, 0.5f, 0.5f)), physicsSimulator->getInstance().createMaterial(0.5f, 0.5f, 0.5f), 1.0f, 0.25f, &lastAddedBox);
		cout << "Box instantiated at: (" << boxPosition.x << ", " << boxPosition.y << ", " << boxPosition.z << ")" << endl;
	}
}

static void cursorpos_callback(GLFWwindow* window, double xpos, double ypos)
{
	return;

	horizontalAngle += mouseSpeed * deltaTime * float(width/2 - xpos);
	verticalAngle += mouseSpeed * deltaTime * float(height/2 - ypos);
	
	// Reset the mouse position to the centre of the window each frame
	glfwSetCursorPos(window, midWindowX, midWindowY);

	cout << "(" << eye.x << ", " << eye.y << ", " << eye.z << ") - H: " << horizontalAngle << " - V: " << verticalAngle << endl;
}

void createBilateralFilter1DKernel(int kernelCenter)
{
	for(int i = 0; i <= kernelCenter; i++)
	{
		bilateralFilter1DKernel[kernelCenter+i] = bilateralFilter1DKernel[kernelCenter-i] =
			0.39894f*exp(-0.5f*i*i/(bilateralFilterSpatialSigma*bilateralFilterSpatialSigma))/bilateralFilterSpatialSigma;
	}
}

void initFrameBuffers()
{
	// FBO for background
	glGenFramebuffers(1, &backgroundFrameBuffer);
	glBindFramebuffer(GL_FRAMEBUFFER, backgroundFrameBuffer);

	glGenTextures(1, &backgroundTexture);
	glBindTexture(GL_TEXTURE_2D, backgroundTexture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 0);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, WINDOW_INITIAL_WIDTH, WINDOW_INITIAL_HEIGHT, 0, GL_RGB, GL_FLOAT, NULL);

	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, backgroundTexture, 0);

	glGenTextures(1, &backgroundDepthTexture);
	glBindTexture(GL_TEXTURE_2D, backgroundDepthTexture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, WINDOW_INITIAL_WIDTH, WINDOW_INITIAL_HEIGHT, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
	
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, backgroundDepthTexture, 0);

	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
	{
		cout << "Couldn't create background frame buffer" << endl;
	}

	glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);  

	// FBO for particles depth
	glGenFramebuffers(1, &depthFrameBuffer);
	glBindFramebuffer(GL_FRAMEBUFFER, depthFrameBuffer);

	glGenTextures(1, &depthTexture);
	glBindTexture(GL_TEXTURE_2D, depthTexture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 0);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, WINDOW_INITIAL_WIDTH, WINDOW_INITIAL_HEIGHT, 0, GL_RGBA, GL_FLOAT, NULL);

	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, depthTexture, 0);

	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
	{
		cout << "Couldn't create particle depth frame buffer" << endl;
	}

	// FBO for particles smoothed depth
	glGenFramebuffers(1, &smoothedDepthFrameBuffer[0]);
	glBindFramebuffer(GL_FRAMEBUFFER, smoothedDepthFrameBuffer[0]);

	glGenTextures(1, &smoothedDepthTexture[0]);
	glBindTexture(GL_TEXTURE_2D, smoothedDepthTexture[0]);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 0);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, WINDOW_INITIAL_WIDTH, WINDOW_INITIAL_HEIGHT, 0, GL_RGBA, GL_FLOAT, NULL);

	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, smoothedDepthTexture[0], 0);

	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
	{
		cout << "Couldn't create smoothed depth buffer 0" << endl;
	}

	glGenFramebuffers(1, &smoothedDepthFrameBuffer[1]);
	glBindFramebuffer(GL_FRAMEBUFFER, smoothedDepthFrameBuffer[1]);

	glGenTextures(1, &smoothedDepthTexture[1]);
	glBindTexture(GL_TEXTURE_2D, smoothedDepthTexture[1]);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 1);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, WINDOW_INITIAL_WIDTH, WINDOW_INITIAL_HEIGHT, 0, GL_RGBA, GL_FLOAT, NULL);

	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, smoothedDepthTexture[1], 0);

	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
	{
		cout << "Couldn't create smoothed depth buffer 1" << endl;
	}
	
	// FBO for foam depth
	glGenFramebuffers(1, &foamDepthFrameBuffer);
	glBindFramebuffer(GL_FRAMEBUFFER, foamDepthFrameBuffer);

	glGenTextures(1, &foamDepthTexture);
	glBindTexture(GL_TEXTURE_2D, foamDepthTexture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 0);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, WINDOW_INITIAL_WIDTH, WINDOW_INITIAL_HEIGHT, 0, GL_RGBA, GL_FLOAT, NULL);

	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, foamDepthTexture, 0);

	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
	{
		cout << "Couldn't create foam depth frame buffer" << endl;
	}

	// FBO for particles depth stencil
	glGenFramebuffers(1, &depthStencilFrameBuffer);
	glBindFramebuffer(GL_FRAMEBUFFER, depthStencilFrameBuffer);

	glGenTextures(1, &depthStencilTexture);
	glBindTexture(GL_TEXTURE_2D, depthStencilTexture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 0);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, WINDOW_INITIAL_WIDTH, WINDOW_INITIAL_HEIGHT, 0, GL_RGBA, GL_FLOAT, NULL);

	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, depthStencilTexture, 0);

	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
	{
		cout << "Couldn't create depth stencil frame buffer" << endl;
	}

	// FBO for foam depth stencil
	glGenFramebuffers(1, &foamDepthStencilFrameBuffer);
	glBindFramebuffer(GL_FRAMEBUFFER, foamDepthStencilFrameBuffer);

	glGenTextures(1, &foamDepthStencilTexture);
	glBindTexture(GL_TEXTURE_2D, foamDepthStencilTexture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 0);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, WINDOW_INITIAL_WIDTH, WINDOW_INITIAL_HEIGHT, 0, GL_RGBA, GL_FLOAT, NULL);

	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, foamDepthStencilTexture, 0);

	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
	{
		cout << "Couldn't create foam depth stencil frame buffer" << endl;
	}

	// FBO for particles thickness
	glGenFramebuffers(1, &thicknessFrameBuffer);
	glBindFramebuffer(GL_FRAMEBUFFER, thicknessFrameBuffer);

	glGenTextures(1, &thicknessTexture);
	glBindTexture(GL_TEXTURE_2D, thicknessTexture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 0);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, WINDOW_INITIAL_WIDTH, WINDOW_INITIAL_HEIGHT, 0, GL_RGBA, GL_FLOAT, NULL);

	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, thicknessTexture, 0);

	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
	{
		cout << "Couldn't create particle thickness frame buffer" << endl;
	}

	glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
}

// Loads a cubemap texture from 6 individual texture faces
// Order should be:
// +X (right)
// -X (left)
// +Y (top)
// -Y (bottom)
// +Z (front) 
// -Z (back)
GLuint loadCubemap(vector<const char*> faces)
{
    GLuint textureID;
    glGenTextures(1, &textureID);

    int width,height;
    unsigned char* image;
    
    glBindTexture(GL_TEXTURE_CUBE_MAP, textureID);

    for(GLuint i = 0; i < faces.size(); i++)
    {
		FREE_IMAGE_FORMAT fif = FIF_UNKNOWN;

		// check the file signature and deduce its format
		fif = FreeImage_GetFileType(faces[i]);

		if (fif == FIF_UNKNOWN) // No signature? Try to guess the file format from the file extension
			fif = FreeImage_GetFIFFromFilename(faces[i]);

		if ((fif == FIF_UNKNOWN) || !FreeImage_FIFSupportsReading(fif))
		{
			cerr << "Texture " << faces[i] << " could not be loaded!" << endl;
			exit(EXIT_FAILURE);
		}

		FIBITMAP *texture = FreeImage_Load(fif, faces[i]);

		if(!texture)
		{
			cerr << "Texture " << faces[i] << " contains invalid data!" << endl;
			exit(EXIT_FAILURE);
		}
		
		FIBITMAP *texture32bits = FreeImage_ConvertTo32Bits(texture);

        glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGBA8, FreeImage_GetWidth(texture), FreeImage_GetHeight(texture), 0, GL_BGRA, GL_UNSIGNED_BYTE, (void*)FreeImage_GetBits(texture32bits));
        FreeImage_Unload(texture);
		FreeImage_Unload(texture32bits);
    }

    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
    glBindTexture(GL_TEXTURE_CUBE_MAP, 0);

    return textureID;
}

GLuint loadTexture(const char* texturePath)
{
	FREE_IMAGE_FORMAT fif = FIF_UNKNOWN;

	// check the file signature and deduce its format
	fif = FreeImage_GetFileType(texturePath);

	if (fif == FIF_UNKNOWN) // No signature? Try to guess the file format from the file extension
		fif = FreeImage_GetFIFFromFilename(texturePath);

	if ((fif == FIF_UNKNOWN) || !FreeImage_FIFSupportsReading(fif))
	{
		cerr << "Texture " << texturePath << " could not be loaded!" << endl;
		exit(EXIT_FAILURE);
	}

	FIBITMAP *texture = FreeImage_Load(fif, texturePath);

	if(!texture)
	{
		cerr << "Texture " << texturePath << " contains invalid data!" << endl;
		exit(EXIT_FAILURE);
	}
		
	FIBITMAP *texture32bits = FreeImage_ConvertTo32Bits(texture);

	GLuint glTexture;
	glGenTextures(1, &glTexture);
	glBindTexture(GL_TEXTURE_2D, glTexture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, FreeImage_GetWidth(texture), FreeImage_GetHeight(texture), 0, GL_BGRA, GL_UNSIGNED_BYTE, (void*)FreeImage_GetBits(texture32bits));
	glGenerateMipmap(GL_TEXTURE_2D);
    FreeImage_Unload(texture);
    FreeImage_Unload(texture32bits);

	glBindTexture(GL_TEXTURE_2D, 0);

	return glTexture;
}

int main(void)
{
	// Setup OpenGL with GLFW and GLEW
	glfwSetErrorCallback(error_callback);

	if (!glfwInit())
	{
		cout << "Failed to initialize GLFW." << endl;
		exit(EXIT_FAILURE);
	}

	window = glfwCreateWindow(WINDOW_INITIAL_WIDTH, WINDOW_INITIAL_HEIGHT, "Water Flow", NULL, NULL);

	if (!window)
	{
		cout << "Failed to create GLFW." << endl;
		glfwTerminate();
		exit(EXIT_FAILURE);
	}

	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_HIDDEN);
	glfwMakeContextCurrent(window);
	glfwSetKeyCallback(window, key_callback);
	glfwSetMouseButtonCallback(window, mousebutton_callback);
	glfwSetCursorPosCallback(window, cursorpos_callback);
	glfwSetCursorPos(window, midWindowX, midWindowY);

	GLenum glewInitResult = glewInit();

	if(glewInitResult != GLEW_OK)
	{
		cout << glewGetErrorString(glewInitResult) << endl;
		exit(EXIT_FAILURE);
	}

	// Setup FreeImage (used to load textures)
#ifdef FREEIMAGE_LIB
	FreeImage_Initialise();
#endif // FREEIMAGE_LIB
	FreeImage_SetOutputMessage(FreeImageErrorHandler);

	// Setup PhysX
	if(!physicsSimulator->getInstance().initPhysicsSimulation(glfwGetCurrentContext()))
	{
		cout << "Error creating PhysX3 device. Exiting..." << endl;
		exit(EXIT_FAILURE);
	}

#ifdef _DEBUG
	// Connecting to PhysX Visual Debugger
	if(!physicsSimulator->getInstance().mPhysics->getPvdConnectionManager())
	{
		cout << "Warning: PhysX Visual Debugger not found running!" << endl;
	}
	else
	{
		debugger::comm::PvdConnection* theConnection = PxVisualDebuggerExt::createConnection( physicsSimulator->getInstance().mPhysics->getPvdConnectionManager(), "127.0.0.1", 5425, 100, PxVisualDebuggerExt::getAllConnectionFlags() );
		
		if (theConnection)
		{
			cout << "Connected to PhysX Visual Debugger!" << endl;
			
			physicsSimulator->getInstance().mPhysics->getVisualDebugger()->setVisualizeConstraints(true);
			physicsSimulator->getInstance().mPhysics->getVisualDebugger()->setVisualDebuggerFlag(PxVisualDebuggerFlag::eTRANSMIT_CONTACTS, true);
		}
	}
#endif

	vector<RenderActor> actors;

	// Screen aligned quad
	GLuint screenQuadVAO;
	glGenVertexArrays(1, &screenQuadVAO);

	GLuint screenQuadVBO;
	glGenBuffers(1, &screenQuadVBO);

	glBindVertexArray(screenQuadVAO);
		glBindBuffer(GL_ARRAY_BUFFER, screenQuadVBO);
		glBufferData(GL_ARRAY_BUFFER, sizeof(screenAlignedQuad), screenAlignedQuad, GL_STATIC_DRAW);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3*sizeof(GLfloat), BUFFER_OFFSET(0));
		glEnableVertexAttribArray(0);
	glBindVertexArray(0);

	// Cubemap (Skybox)
	// Cartoon texture
    vector<const char*> faces;
    faces.push_back("..\\WaterFlow\\media\\CartoonSkybox\\right.png");
    faces.push_back("..\\WaterFlow\\media\\CartoonSkybox\\left.png");
    faces.push_back("..\\WaterFlow\\media\\CartoonSkybox\\top.png");
    faces.push_back("..\\WaterFlow\\media\\CartoonSkybox\\bottom.png");
    faces.push_back("..\\WaterFlow\\media\\CartoonSkybox\\back.png");
    faces.push_back("..\\WaterFlow\\media\\CartoonSkybox\\front.png");
    GLuint cubemapTextureCartoon = loadCubemap(faces);

	// Realistic texture
	faces.clear();
    faces.push_back("..\\WaterFlow\\media\\RealisticSkybox\\right.tga");
    faces.push_back("..\\WaterFlow\\media\\RealisticSkybox\\left.tga");
    faces.push_back("..\\WaterFlow\\media\\RealisticSkybox\\top.tga");
    faces.push_back("..\\WaterFlow\\media\\RealisticSkybox\\bottom.tga");
    faces.push_back("..\\WaterFlow\\media\\RealisticSkybox\\back.tga");
    faces.push_back("..\\WaterFlow\\media\\RealisticSkybox\\front.tga");
    GLuint cubemapTextureRealistic = loadCubemap(faces);

	GLuint skyboxVAO;
	glGenVertexArrays(1, &skyboxVAO);

	GLuint skyboxVBO;
	glGenBuffers(1, &skyboxVBO);

	glBindVertexArray(skyboxVAO);
		glBindBuffer(GL_ARRAY_BUFFER, skyboxVBO);
		glBufferData(GL_ARRAY_BUFFER, sizeof(skyboxVertices), &skyboxVertices, GL_STATIC_DRAW);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3*sizeof(GLfloat), BUFFER_OFFSET(0)); // Positions
		glEnableVertexAttribArray(0);
	glBindVertexArray(0);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	
	// Box texture cartoon
	faces.clear();
    faces.push_back("..\\WaterFlow\\media\\container.jpg");
    faces.push_back("..\\WaterFlow\\media\\container.jpg");
    faces.push_back("..\\WaterFlow\\media\\container.jpg");
    faces.push_back("..\\WaterFlow\\media\\container.jpg");
    faces.push_back("..\\WaterFlow\\media\\container.jpg");
    faces.push_back("..\\WaterFlow\\media\\container.jpg");
    GLuint boxTexture = loadCubemap(faces);

	// Load heightmap
	RenderActor hfStructure = physicsSimulator->getInstance().addHeightfield("..\\WaterFlow\\media\\terrain\\heightmap.jpg", -10.0f, 1.0f, 1.0f, 0.125f, physicsSimulator->getInstance().createMaterial(0.9f, 0.9f, 0.001f));
	GLuint heightmapGrassTexture = loadTexture("..\\WaterFlow\\media\\grass.jpg");
	GLuint heightmapGroundTexture = loadTexture("..\\WaterFlow\\media\\ground.jpg");

	// Create big box
	bigBoxStructure = physicsSimulator->getInstance().addBox(BIG_BOX_POSITION, PxBoxGeometry(BIG_BOX_SIZE), physicsSimulator->getInstance().createMaterial(0.5f, 0.5f, 0.5f), 5.0f, 1.5f, &bigBox);
	
	// Create particle fluid emitter
	ParticleFluidEmitter* particleFluidEmitter = physicsSimulator->getInstance().addParticleFluidEmitter(MAX_PARTICLES, PxVec3(54.3214f, 30.0006f, -56.1342f), PARTICLES_RELEASE_DELAY, 0.3f, 5.0f, 0.1f, 0.01f, PARTICLES_DISTANCE);
	
	GLuint particleFluidEmitterVBO;
	glGenBuffers(1, &particleFluidEmitterVBO);

	double lastTime = glfwGetTime();
	int nbFrames = 0;
	double physicsComputingTimeAccumulator = 0;

	int nbComputingTimeCalculations = 0;
	double physicsComputingTimeTotal = 0;

	double totalRenderingTime = 0;
	double averageRenderingTimeAfter2Minutes = 0;

	float aspect;

	mat4 viewMatrix;
	mat4 projectionMatrix;

	// Enables depth test and backface culling, to prevent the cube faces from "merging" with one another
	glEnable(GL_DEPTH_TEST);
	glClearDepth(1.0f);
	glDepthFunc(GL_LESS);
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);

	initFrameBuffers();

	GLuint vPosition; // TODO: apagar

	while (!glfwWindowShouldClose(window))
	{
		glfwPollEvents();

		// Calculate the time needed to draw a frame
		// 60 FPS is approximately 16.6666ms
		// 30 FPS is approximately 33.3333ms
		double currentTime = glfwGetTime();

		nbFrames++;

		deltaTime = currentTime - lastTime;
		if(deltaTime >= 1.0)
		{
			// TODO: render on screen instead of writing on console
			// print
			nbComputingTimeCalculations++;
			cout << 1000.0/double(nbFrames) << "ms/frame (" << nbFrames << " FPS) - " << particleFluidEmitter->numberActiveParticles << " particles." << endl;
		}

		double physicsComputingBegin = glfwGetTime();

		// Step the physics simulation
		if(physicsSimulator->getInstance().isRunning && glfwGetTime() < 300)
		{
			physicsSimulator->getInstance().stepPhysicsSimulation();
			particleFluidEmitter->upDate(deltaTime);
		}

		double physicsComputingTimeEnd = glfwGetTime();
		physicsComputingTimeAccumulator += physicsComputingTimeEnd - physicsComputingBegin;
		physicsComputingTimeTotal += physicsComputingTimeEnd - physicsComputingBegin;

		if(deltaTime >= 1.0)
		{
			// TODO: render on screen instead of writing on console
			// print phyisics computing time
			cout << "- " << physicsComputingTimeAccumulator * (1000.0/double(nbFrames)) << "ms/frame for physics. " << endl;
		}

		// Rendering
		glfwGetFramebufferSize(window, &width, &height);
		glViewport(0, 0, width, height);

		midWindowX = width/2;
		midWindowY = height/2;

		// Compute camera rotation (position is computed on the keyboard callback)
		center = vec3(cos(verticalAngle) * sin(horizontalAngle), sin(verticalAngle), cos(verticalAngle) * cos(horizontalAngle));
		rightVector = vec3(sin(horizontalAngle - 3.14f/2.0f), 0.0f, cos(horizontalAngle - 3.14/2.0f));
		upVector = cross(rightVector, center);

		// Recalculate projection and view matrixes
		projectionMatrix = perspective(fovy, width/(float)height, near, far);
		viewMatrix = lookAt(eye, eye+center, upVector);

		glEnable(GL_DEPTH_TEST);
		glDisable(GL_CULL_FACE);
		glDisable(GL_POINT_SPRITE);

		// 2. Background rendering ------------------------------------------------------------------------------------------------------------------------------------------------
		glBindFramebuffer(GL_FRAMEBUFFER, whatToRender == RENDER_BACKGROUND_ONLY ? 0 : backgroundFrameBuffer);
		glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

		// Skybox
		glDepthMask(GL_FALSE);
		glUseProgram(particleFluidEmitter->programs[SHADER_SKYBOX]);
			
		glUniformMatrix4fv( glGetUniformLocation( particleFluidEmitter->programs[SHADER_SKYBOX], "View" ), 1, GL_FALSE, value_ptr(mat4(mat3(viewMatrix))));
		glUniformMatrix4fv( glGetUniformLocation( particleFluidEmitter->programs[SHADER_SKYBOX], "Projection" ), 1, GL_FALSE, value_ptr(projectionMatrix));

		glActiveTexture(GL_TEXTURE0);
		if (renderingStyle == NONPHOTOREALISTIC_RENDERING) glBindTexture(GL_TEXTURE_CUBE_MAP, cubemapTextureCartoon); else glBindTexture(GL_TEXTURE_CUBE_MAP, cubemapTextureRealistic);
		glUniform1i( glGetUniformLocation( particleFluidEmitter->programs[SHADER_SKYBOX], "Skybox" ), 0);

		glBindVertexArray(skyboxVAO);
		glDrawArrays(GL_TRIANGLES, 0, 36);
		glBindVertexArray(0);

		glDepthMask(GL_TRUE);

		// Actors
		vector<RenderActor> actors = physicsSimulator->getInstance().getActors();
		for(vector<RenderActor>::iterator actor = actors.begin(); actor != actors.end(); actor++)
		{
			PxU32 nbShapes;
			PxShape** shapes;

			if(actor->renderActor->isRigidDynamic())
			{
				nbShapes = ((PxRigidDynamic*)actor->renderActor)->getNbShapes();
				shapes = new PxShape*[nbShapes];

				((PxRigidDynamic*)actor->renderActor)->getShapes(shapes, nbShapes);
			}
			
			if(actor->renderActor->isRigidStatic())
			{
				nbShapes = ((PxRigidStatic*)actor->renderActor)->getNbShapes();
				shapes = new PxShape*[nbShapes];
				((PxRigidStatic*)actor->renderActor)->getShapes(shapes, nbShapes);
			}

			while(nbShapes--)
			{
				PxMat44 modelMatrix;
				PxTransform transform = PxShapeExt::getGlobalPose(*shapes[nbShapes], *shapes[nbShapes]->getActor());
				modelMatrix = PxMat44(transform);

				switch(shapes[nbShapes]->getGeometryType())
				{
					case PxGeometryType::eBOX:
						{
							glUseProgram(actor->programs[0]);

							glUniformMatrix4fv( glGetUniformLocation( actor->programs[0], "Model" ), 1, GL_FALSE, modelMatrix.front());
							glUniformMatrix4fv( glGetUniformLocation( actor->programs[0], "View" ), 1, GL_FALSE, value_ptr(viewMatrix) );
							glUniformMatrix4fv( glGetUniformLocation( actor->programs[0], "Projection" ), 1, GL_FALSE, value_ptr(projectionMatrix) );
							glUniform4fv( glGetUniformLocation( actor->programs[0], "LightPosition" ), 1, value_ptr(lightPosition) );
							glUniform3fv( glGetUniformLocation( actor->programs[0], "LightIntensity" ), 1, value_ptr(lightIntensity) );
							glUniform1i( glGetUniformLocation( actor->programs[0], "PhotoRealistic" ), renderingStyle);

							glActiveTexture(GL_TEXTURE0);
							glBindTexture(GL_TEXTURE_CUBE_MAP, boxTexture);
							glUniform1i( glGetUniformLocation( actor->programs[0], "Texture" ), 0);

							glBindVertexArray(actor->VAO);
							glDrawElements( GL_TRIANGLE_STRIP, actor->indicesCount, GL_UNSIGNED_INT, 0 );
							glBindVertexArray(0);
						}
						break;
					case PxGeometryType::eHEIGHTFIELD:
						{
							glUseProgram(actor->programs[0]);

							glUniformMatrix4fv( glGetUniformLocation( actor->programs[0], "Model" ), 1, GL_FALSE, modelMatrix.front());
							glUniformMatrix4fv( glGetUniformLocation( actor->programs[0], "View" ), 1, GL_FALSE, value_ptr(viewMatrix) );
							glUniformMatrix4fv( glGetUniformLocation( actor->programs[0], "Projection" ), 1, GL_FALSE, value_ptr(projectionMatrix) );
							glUniform4fv( glGetUniformLocation( actor->programs[0], "LightPosition" ), 1, value_ptr(lightPosition) );
							glUniform3fv( glGetUniformLocation( actor->programs[0], "LightIntensity" ), 1, value_ptr(lightIntensity) );
							glUniform1i( glGetUniformLocation( actor->programs[0], "Texture" ), 0);
							glUniform1i( glGetUniformLocation( actor->programs[0], "PhotoRealistic" ), renderingStyle);

							glActiveTexture(GL_TEXTURE0);
							glBindTexture(GL_TEXTURE_2D, heightmapGrassTexture);
							glUniform1i( glGetUniformLocation( actor->programs[0], "GrassTexture" ), 0);

							glActiveTexture(GL_TEXTURE1);
							glBindTexture(GL_TEXTURE_2D, heightmapGroundTexture);
							glUniform1i( glGetUniformLocation( actor->programs[0], "GroundTexture" ), 1);
							
							glBindVertexArray(actor->VAO);
							glDrawElements(GL_TRIANGLES, actor->indicesCount, GL_UNSIGNED_INT, 0);
							glBindVertexArray(0);
						}
						break;
				}
			}
		}

		glEnable(GL_CULL_FACE);
		glEnable(GL_VERTEX_PROGRAM_POINT_SIZE);
		glEnable(GL_POINT_SPRITE);

		// 3. Fluid rendering ------------------------------------------------------------------------------------------------------------------------------------------------
		if (particleFluidEmitter)
		{
			glBindBuffer(GL_ARRAY_BUFFER, particleFluidEmitterVBO);
			glBufferData( GL_ARRAY_BUFFER, (particleFluidEmitter->numberActiveParticles * sizeof(vec4) + particleFluidEmitter->numberActiveParticles * sizeof(PxF32)), NULL, GL_STATIC_DRAW );
			glBufferSubData( GL_ARRAY_BUFFER, 0, particleFluidEmitter->numberActiveParticles * sizeof(vec4), particleFluidEmitter->particlePositions );

			if(foamDropletGenerationMethod == WEBERNUMBER_FOAMANDDROPLET)
			{
				glBufferSubData( GL_ARRAY_BUFFER, particleFluidEmitter->numberActiveParticles * sizeof(vec4), particleFluidEmitter->numberActiveParticles * sizeof(PxF32), particleFluidEmitter->particleWeberNumbers );
			}
			else if (foamDropletGenerationMethod == DENSITY_FOAMANDDROPLET)
			{
				glBufferSubData( GL_ARRAY_BUFFER, particleFluidEmitter->numberActiveParticles * sizeof(vec4), particleFluidEmitter->numberActiveParticles * sizeof(PxF32), particleFluidEmitter->particleDensities );
			}

			glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0));
			glEnableVertexAttribArray(0);
			glVertexAttribPointer(1, 1, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(particleFluidEmitter->numberActiveParticles * sizeof(vec4)));
			glEnableVertexAttribArray(1);
			glVertexAttribDivisor(0, 1);
			glVertexAttribDivisor(1, 1);

			int pingPong = 0;
			mat4 modelViewMatrix = mat4() * viewMatrix;
			float particleSizeFactor = 1.5f; // The particle gets bigger as the particleSizeFactor value decrease

			// 3.0. Particle cloud ------------------------------------------------------------------------------------------------------------------------------------------------
			if (whatToRender == RENDER_PARTICLE_CLOUD)
			{
				glBindFramebuffer(GL_FRAMEBUFFER, 0);

				glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
				glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

				glUseProgram(particleFluidEmitter->programs[SHADER_PARTICLE_CLOUD]);

				glUniformMatrix4fv( glGetUniformLocation( particleFluidEmitter->programs[SHADER_PARTICLE_CLOUD], "ModelView" ), 1, GL_FALSE, value_ptr(modelViewMatrix));
				glUniformMatrix4fv( glGetUniformLocation( particleFluidEmitter->programs[SHADER_PARTICLE_CLOUD], "Projection" ), 1, GL_FALSE, value_ptr(projectionMatrix) );
				glUniform2f( glGetUniformLocation( particleFluidEmitter->programs[SHADER_PARTICLE_CLOUD], "ScreenResolution" ), width, height);
				glUniform1f( glGetUniformLocation( particleFluidEmitter->programs[SHADER_PARTICLE_CLOUD], "ParticleSizeFactor" ), particleSizeFactor);
				glUniform1f( glGetUniformLocation( particleFluidEmitter->programs[SHADER_PARTICLE_CLOUD], "Near" ), near);
				glUniform1f( glGetUniformLocation( particleFluidEmitter->programs[SHADER_PARTICLE_CLOUD], "Far" ), far);

				glDrawArraysInstanced(GL_POINTS, 0, 1, particleFluidEmitter->numberActiveParticles);
			}

			// 3.1. Particle depth ------------------------------------------------------------------------------------------------------------------------------------------------
			glBindFramebuffer(GL_FRAMEBUFFER, whatToRender == RENDER_PARTICLEDEPTH_ONLY ? 0 : depthFrameBuffer);
			glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

			glUseProgram(particleFluidEmitter->programs[SHADER_DEPTH]);

			glUniformMatrix4fv( glGetUniformLocation( particleFluidEmitter->programs[SHADER_DEPTH], "ModelView" ), 1, GL_FALSE, value_ptr(modelViewMatrix));
			glUniformMatrix4fv( glGetUniformLocation( particleFluidEmitter->programs[SHADER_DEPTH], "Projection" ), 1, GL_FALSE, value_ptr(projectionMatrix) );
			glUniform2f( glGetUniformLocation( particleFluidEmitter->programs[SHADER_DEPTH], "ScreenResolution" ), width, height);
			glUniform1f( glGetUniformLocation( particleFluidEmitter->programs[SHADER_DEPTH], "ParticleSizeFactor" ), particleSizeFactor);
			glUniform1f( glGetUniformLocation( particleFluidEmitter->programs[SHADER_DEPTH], "Near" ), near);
			glUniform1f( glGetUniformLocation( particleFluidEmitter->programs[SHADER_DEPTH], "Far" ), far);
			glUniform1f( glGetUniformLocation( particleFluidEmitter->programs[SHADER_DEPTH], "FoamThreshold" ), foamThreshold);

			glDrawArraysInstanced(GL_POINTS, 0, 1, particleFluidEmitter->numberActiveParticles);

			// 3.2. Foam depth ------------------------------------------------------------------------------------------------------------------------------------------------
			glBindFramebuffer(GL_FRAMEBUFFER, whatToRender == RENDER_PARTICLEFOAM_ONLY ? 0 : foamDepthFrameBuffer);
			glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

			glUseProgram(particleFluidEmitter->programs[SHADER_FOAM_DEPTH]);

			glUniformMatrix4fv( glGetUniformLocation( particleFluidEmitter->programs[SHADER_FOAM_DEPTH], "ModelView" ), 1, GL_FALSE, value_ptr(modelViewMatrix));
			glUniformMatrix4fv( glGetUniformLocation( particleFluidEmitter->programs[SHADER_FOAM_DEPTH], "Projection" ), 1, GL_FALSE, value_ptr(projectionMatrix) );
			glUniform2f( glGetUniformLocation( particleFluidEmitter->programs[SHADER_FOAM_DEPTH], "ScreenResolution" ), width, height);
			glUniform1f( glGetUniformLocation( particleFluidEmitter->programs[SHADER_FOAM_DEPTH], "ParticleSizeFactor" ), particleSizeFactor);
			glUniform1f( glGetUniformLocation( particleFluidEmitter->programs[SHADER_FOAM_DEPTH], "Near" ), near);
			glUniform1f( glGetUniformLocation( particleFluidEmitter->programs[SHADER_FOAM_DEPTH], "Far" ), far);
			glUniform1f( glGetUniformLocation( particleFluidEmitter->programs[SHADER_FOAM_DEPTH], "FoamThreshold" ), foamThreshold);

			glDrawArraysInstanced(GL_POINTS, 0, 1, particleFluidEmitter->numberActiveParticles);

			// 3.3. Particle thickness ------------------------------------------------------------------------------------------------------------------------------------------------
			glEnable(GL_BLEND);
			glBlendFunc(GL_ONE, GL_ONE);
			glDisable(GL_DEPTH_TEST);

			glBindFramebuffer(GL_FRAMEBUFFER, whatToRender == RENDER_PARTICLETHICKNESS_ONLY ? 0 : thicknessFrameBuffer);
			glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

			glUseProgram(particleFluidEmitter->programs[SHADER_THICKNESS]);

			glUniformMatrix4fv( glGetUniformLocation( particleFluidEmitter->programs[SHADER_THICKNESS], "ModelView" ), 1, GL_FALSE, value_ptr(modelViewMatrix));
			glUniformMatrix4fv( glGetUniformLocation( particleFluidEmitter->programs[SHADER_THICKNESS], "Projection" ), 1, GL_FALSE, value_ptr(projectionMatrix) );
			glUniform2f( glGetUniformLocation( particleFluidEmitter->programs[SHADER_THICKNESS], "ScreenResolution" ), width, height);
			glUniform1f( glGetUniformLocation( particleFluidEmitter->programs[SHADER_THICKNESS], "ParticleSizeFactor" ), particleSizeFactor);
			
			glDrawArraysInstanced(GL_POINTS, 0, 1, particleFluidEmitter->numberActiveParticles);
			glBindVertexArray(0);

			glDisable(GL_BLEND); 

			// 3.4. Depth smoothing ------------------------------------------------------------------------------------------------------------------------------------------------
			glDisable(GL_DEPTH_TEST);

			if (smoothingMethod == CURVATURE_FLOW)
			{
				glUseProgram(particleFluidEmitter->programs[SHADER_CURVATURE_FLOW_FILTER]);

				for (int i = 0; i < curvatureFlowNumberOfIterations; i++)
				{
					glBindFramebuffer(GL_FRAMEBUFFER, whatToRender == RENDER_PARTICLESMOOTHEDDEPTH_ONLY && i == curvatureFlowNumberOfIterations - 1 ? 0 : smoothedDepthFrameBuffer[pingPong]);
					glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
					glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

					glActiveTexture(GL_TEXTURE0);
					if(i == 0) glBindTexture(GL_TEXTURE_2D, depthTexture); else glBindTexture(GL_TEXTURE_2D, smoothedDepthTexture[1 - pingPong]);
					glUniform1i( glGetUniformLocation( particleFluidEmitter->programs[SHADER_CURVATURE_FLOW_FILTER], "DepthTexture" ), 0);
					glUniform2f( glGetUniformLocation( particleFluidEmitter->programs[SHADER_CURVATURE_FLOW_FILTER], "ScreenResolution" ), width, height);
					glUniformMatrix4fv( glGetUniformLocation( particleFluidEmitter->programs[SHADER_CURVATURE_FLOW_FILTER], "Projection" ), 1, GL_FALSE, value_ptr(projectionMatrix) );
					glUniform1f( glGetUniformLocation( particleFluidEmitter->programs[SHADER_CURVATURE_FLOW_FILTER], "Fovy" ), fovy );
					glUniform1f( glGetUniformLocation( particleFluidEmitter->programs[SHADER_CURVATURE_FLOW_FILTER], "Epsilon" ), curvatureFlowEpsilon );
					
					glBindVertexArray(screenQuadVAO);
					glDrawArrays(GL_TRIANGLES, 0, 6);
					glBindVertexArray(0);

					pingPong = 1 - pingPong;
				}
			}
			
			if (smoothingMethod == FULLKERNEL_BILATERAL_FILTER)
			{
				float bilateralFilterDirection[2];
			
				glUseProgram(particleFluidEmitter->programs[SHADER_BILATERAL_FILTER]);
			
				int bilateralFilterKernelCenter = (bilateralFilterKernelSize-1)/2;
				createBilateralFilter1DKernel(bilateralFilterKernelCenter);

				// Single pass
				bilateralFilterDirection[0] = 1.0;
				bilateralFilterDirection[1] = 1.0;

				glBindFramebuffer(GL_FRAMEBUFFER, whatToRender == RENDER_PARTICLESMOOTHEDDEPTH_ONLY ? 0 : smoothedDepthFrameBuffer[0]);
				glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
				glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

				glActiveTexture(GL_TEXTURE0);
				glBindTexture(GL_TEXTURE_2D, depthTexture);
				glUniform1i( glGetUniformLocation( particleFluidEmitter->programs[SHADER_BILATERAL_FILTER], "DepthTexture" ), 0);
				glUniform2f( glGetUniformLocation( particleFluidEmitter->programs[SHADER_BILATERAL_FILTER], "Direction" ), bilateralFilterDirection[0], bilateralFilterDirection[1]);
				glUniform2f( glGetUniformLocation( particleFluidEmitter->programs[SHADER_BILATERAL_FILTER], "ScreenResolution" ), width, height);
				glUniform1fv( glGetUniformLocation( particleFluidEmitter->programs[SHADER_BILATERAL_FILTER], "Kernel" ), MAX_BILATERAL_FILTER_KERNEL_SIZE, bilateralFilter1DKernel);
				glUniform1f( glGetUniformLocation( particleFluidEmitter->programs[SHADER_BILATERAL_FILTER], "DomainSigma" ), bilateralFilterDomainSigma);
				glUniform1i( glGetUniformLocation( particleFluidEmitter->programs[SHADER_BILATERAL_FILTER], "KernelCenter" ), bilateralFilterKernelCenter);
					
				glBindVertexArray(screenQuadVAO);
				glDrawArrays(GL_TRIANGLES, 0, 6);
				glBindVertexArray(0);
			}

			if (smoothingMethod == SEPARATED_BILATERAL_FILTER)
			{
				float bilateralFilterDirection[2];
			
				glUseProgram(particleFluidEmitter->programs[SHADER_BILATERAL_FILTER]);
			
				int bilateralFilterKernelCenterDividingFactor = 1;
				int bilateralFilterKernelCenter;

				for(int i = 0; i < separatedIterativeBilateralFilterNumberOfIterations; i++)
				{
					if (i > 0) bilateralFilterKernelCenterDividingFactor *= 2;
					bilateralFilterKernelCenter = ((bilateralFilterKernelSize/bilateralFilterKernelCenterDividingFactor)-1)/2;
					createBilateralFilter1DKernel(bilateralFilterKernelCenter);

					// Horizontal pass
					bilateralFilterDirection[0] = 1.0;
					bilateralFilterDirection[1] = 0.0;

					glBindFramebuffer(GL_FRAMEBUFFER,  whatToRender == RENDER_PARTICLESMOOTHEDDEPTH_ONLY && i == separatedIterativeBilateralFilterNumberOfIterations - 1 ? 0 : smoothedDepthFrameBuffer[1 - pingPong]);
					glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
					glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

					glActiveTexture(GL_TEXTURE0);
					if(i == 0) glBindTexture(GL_TEXTURE_2D, depthTexture); else glBindTexture(GL_TEXTURE_2D, smoothedDepthTexture[pingPong]);
					glUniform1i( glGetUniformLocation( particleFluidEmitter->programs[SHADER_BILATERAL_FILTER], "DepthTexture" ), 0);
					glUniform2f( glGetUniformLocation( particleFluidEmitter->programs[SHADER_BILATERAL_FILTER], "Direction" ), bilateralFilterDirection[0], bilateralFilterDirection[1]);
					glUniform2f( glGetUniformLocation( particleFluidEmitter->programs[SHADER_BILATERAL_FILTER], "ScreenResolution" ), width, height);
					glUniform1fv( glGetUniformLocation( particleFluidEmitter->programs[SHADER_BILATERAL_FILTER], "Kernel" ), MAX_BILATERAL_FILTER_KERNEL_SIZE, bilateralFilter1DKernel);
					glUniform1f( glGetUniformLocation( particleFluidEmitter->programs[SHADER_BILATERAL_FILTER], "DomainSigma" ), bilateralFilterDomainSigma);
					glUniform1i( glGetUniformLocation( particleFluidEmitter->programs[SHADER_BILATERAL_FILTER], "KernelCenter" ), bilateralFilterKernelCenter);
					
					glBindVertexArray(screenQuadVAO);
					glDrawArrays(GL_TRIANGLES, 0, 6);
					glBindVertexArray(0);

					pingPong = 1 - pingPong;
				
					// Vertical pass
					bilateralFilterDirection[0] = 0.0;
					bilateralFilterDirection[1] = 1.0;

					glBindFramebuffer(GL_FRAMEBUFFER, smoothedDepthFrameBuffer[1 - pingPong]);
					glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
					glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

					vPosition = glGetAttribLocation( particleFluidEmitter->programs[SHADER_BILATERAL_FILTER], "vPosition" );
					glEnableVertexAttribArray( vPosition );
					glVertexAttribPointer( vPosition, 3, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0) );

					glActiveTexture(GL_TEXTURE0);
					glBindTexture(GL_TEXTURE_2D, smoothedDepthTexture[pingPong]);
					glUniform1i( glGetUniformLocation( particleFluidEmitter->programs[SHADER_BILATERAL_FILTER], "DepthTexture" ), 0);
					glUniform2f( glGetUniformLocation( particleFluidEmitter->programs[SHADER_BILATERAL_FILTER], "Direction" ), bilateralFilterDirection[0], bilateralFilterDirection[1]);
					glUniform2f( glGetUniformLocation( particleFluidEmitter->programs[SHADER_BILATERAL_FILTER], "ScreenResolution" ), width, height);
					glUniform1fv( glGetUniformLocation( particleFluidEmitter->programs[SHADER_BILATERAL_FILTER], "Kernel" ), MAX_BILATERAL_FILTER_KERNEL_SIZE, bilateralFilter1DKernel);
					glUniform1f( glGetUniformLocation( particleFluidEmitter->programs[SHADER_BILATERAL_FILTER], "DomainSigma" ), bilateralFilterDomainSigma);
					glUniform1i( glGetUniformLocation( particleFluidEmitter->programs[SHADER_BILATERAL_FILTER], "KernelCenter" ), bilateralFilterKernelCenter);
					
					glBindVertexArray(screenQuadVAO);
					glDrawArrays(GL_TRIANGLES, 0, 6);
					glBindVertexArray(0);
				
					pingPong = 1 - pingPong;
				}
			}

			glEnable(GL_DEPTH_TEST);

			// TODO: fazer passo de smoothing da texture de thickness tamb�m? Acho que seria uma boa pra avaliar visualmente e em termos de performance (creio que a performance vai cair razoavelmente com uma pequena melhora visual, mas precisa confirmar)

			// 3.5. Particle depth stencil ------------------------------------------------------------------------------------------------------------------------------------------------
			glBindFramebuffer(GL_FRAMEBUFFER, depthStencilFrameBuffer);
			glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

			glUseProgram(particleFluidEmitter->programs[SHADER_DEPTH_STENCIL]);
			   
			glUniform1f( glGetUniformLocation( particleFluidEmitter->programs[SHADER_DEPTH_STENCIL], "Near" ), near);
			glUniform1f( glGetUniformLocation( particleFluidEmitter->programs[SHADER_DEPTH_STENCIL], "Far" ), far);

			glActiveTexture(GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_2D, depthTexture);
			glUniform1i( glGetUniformLocation( particleFluidEmitter->programs[SHADER_DEPTH_STENCIL], "ParticlesSmoothedDepthTexture" ), 0);

			glActiveTexture(GL_TEXTURE1);
			glBindTexture(GL_TEXTURE_2D, backgroundDepthTexture);
			glUniform1i( glGetUniformLocation( particleFluidEmitter->programs[SHADER_DEPTH_STENCIL], "BackgroundDepthTexture" ), 1);

			glBindVertexArray(screenQuadVAO);
			glDrawArrays(GL_TRIANGLES, 0, 6);
			glBindVertexArray(0);

			// 3.6. Foam depth stencil ------------------------------------------------------------------------------------------------------------------------------------------------
			glBindFramebuffer(GL_FRAMEBUFFER, foamDepthStencilFrameBuffer);
			glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

			glUseProgram(particleFluidEmitter->programs[SHADER_DEPTH_STENCIL]);

			glUniform1f( glGetUniformLocation( particleFluidEmitter->programs[SHADER_DEPTH_STENCIL], "Near" ), near);
			glUniform1f( glGetUniformLocation( particleFluidEmitter->programs[SHADER_DEPTH_STENCIL], "Far" ), far);

			glActiveTexture(GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_2D, foamDepthTexture);
			glUniform1i( glGetUniformLocation( particleFluidEmitter->programs[SHADER_DEPTH_STENCIL], "ParticlesSmoothedDepthTexture" ), 0);

			glActiveTexture(GL_TEXTURE1);
			glBindTexture(GL_TEXTURE_2D, backgroundDepthTexture);
			glUniform1i( glGetUniformLocation( particleFluidEmitter->programs[SHADER_DEPTH_STENCIL], "BackgroundDepthTexture" ), 1);

			glBindVertexArray(screenQuadVAO);
			glDrawArrays(GL_TRIANGLES, 0, 6);
			glBindVertexArray(0);

			if(whatToRender == RENDER_EVERYTHING)
			{
				// Render to screen
				glBindFramebuffer(GL_FRAMEBUFFER, 0);

				glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
				glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

				// 3.8. Final rendering 
				glDisable(GL_DEPTH_TEST);

				glUseProgram(particleFluidEmitter->programs[SHADER_CARTOON_WATER]);

				glUniformMatrix4fv( glGetUniformLocation( particleFluidEmitter->programs[SHADER_CARTOON_WATER], "ModelView" ), 1, GL_FALSE, value_ptr(modelViewMatrix) );
				glUniformMatrix4fv( glGetUniformLocation( particleFluidEmitter->programs[SHADER_CARTOON_WATER], "Projection" ), 1, GL_FALSE, value_ptr(projectionMatrix) );
				glUniform1f( glGetUniformLocation( particleFluidEmitter->programs[SHADER_CARTOON_WATER], "Near" ), near);
				glUniform1f( glGetUniformLocation( particleFluidEmitter->programs[SHADER_CARTOON_WATER], "Far" ), far);
				glUniform2f( glGetUniformLocation( particleFluidEmitter->programs[SHADER_CARTOON_WATER], "ScreenResolution" ), width, height);
				glUniform4fv( glGetUniformLocation( particleFluidEmitter->programs[SHADER_CARTOON_WATER], "LightDir" ), 1, value_ptr(lightPosition) );
				glUniform3fv( glGetUniformLocation( particleFluidEmitter->programs[SHADER_CARTOON_WATER], "LightIntensity" ), 1, value_ptr(lightIntensity) );
				glUniform1i( glGetUniformLocation( particleFluidEmitter->programs[SHADER_CARTOON_WATER], "CartoonLevels" ), 3 );
				glUniform4fv( glGetUniformLocation( particleFluidEmitter->programs[SHADER_CARTOON_WATER], "WaterColor" ), 1, value_ptr(vec4(0.392f, 0.694f, 0.980f, 1.0f)) );
				glUniform1i( glGetUniformLocation( particleFluidEmitter->programs[SHADER_CARTOON_WATER], "PhotoRealistic" ), renderingStyle);
				glUniform1i( glGetUniformLocation( particleFluidEmitter->programs[SHADER_CARTOON_WATER], "OpticalEffects" ), opticalEffectsToRender);
			
				glActiveTexture(GL_TEXTURE0);
				glBindTexture(GL_TEXTURE_2D, backgroundTexture);
				glUniform1i( glGetUniformLocation( particleFluidEmitter->programs[SHADER_CARTOON_WATER], "BackgroundTexture" ), 0);
			
				glActiveTexture(GL_TEXTURE1);
				glBindTexture(GL_TEXTURE_2D, backgroundDepthTexture);
				glUniform1i( glGetUniformLocation( particleFluidEmitter->programs[SHADER_CARTOON_WATER], "BackgroundDepthTexture" ), 1);

				glActiveTexture(GL_TEXTURE2);

				if(smoothingMethod == NO_SMOOTHING)
				{
					glBindTexture(GL_TEXTURE_2D, depthTexture);
				}
				else
				{
					glBindTexture(GL_TEXTURE_2D, smoothedDepthTexture[smoothingMethod == FULLKERNEL_BILATERAL_FILTER ? 0 : 1 - pingPong]);
				}
				glUniform1i( glGetUniformLocation( particleFluidEmitter->programs[SHADER_CARTOON_WATER], "ParticlesSmoothedDepthTexture" ), 2);
			
				glActiveTexture(GL_TEXTURE3);
				glBindTexture(GL_TEXTURE_2D, depthStencilTexture);
				glUniform1i( glGetUniformLocation( particleFluidEmitter->programs[SHADER_CARTOON_WATER], "ParticlesDepthStencilTexture" ), 3);
			
				glActiveTexture(GL_TEXTURE4);
				glBindTexture(GL_TEXTURE_2D, foamDepthTexture);
				glUniform1i( glGetUniformLocation( particleFluidEmitter->programs[SHADER_CARTOON_WATER], "FoamDepthTexture" ), 4);
			
				glActiveTexture(GL_TEXTURE5);
				glBindTexture(GL_TEXTURE_2D, foamDepthStencilTexture);
				glUniform1i( glGetUniformLocation( particleFluidEmitter->programs[SHADER_CARTOON_WATER], "FoamDepthStencilTexture" ), 5);
			
				glActiveTexture(GL_TEXTURE6);
				glBindTexture(GL_TEXTURE_2D, thicknessTexture);
				glUniform1i( glGetUniformLocation( particleFluidEmitter->programs[SHADER_CARTOON_WATER], "ParticlesSmoothedThicknessTexture" ), 6);
			
				glActiveTexture(GL_TEXTURE7);
				if (renderingStyle == NONPHOTOREALISTIC_RENDERING) glBindTexture(GL_TEXTURE_CUBE_MAP, cubemapTextureCartoon); else glBindTexture(GL_TEXTURE_CUBE_MAP, cubemapTextureRealistic);
				glUniform1i( glGetUniformLocation( particleFluidEmitter->programs[SHADER_CARTOON_WATER], "EnvironmentTexture" ), 7);

				glBindVertexArray(screenQuadVAO);
				glDrawArrays(GL_TRIANGLES, 0, 6);
				glBindVertexArray(0);
			}
		}

		glDisable(GL_VERTEX_PROGRAM_POINT_SIZE);

		if(deltaTime >= 1.0)
		{
			// TODO: render on screen instead of writing on console
			// print render time and reset variables
			double renderingTime = (1000.0/double(nbFrames)) - (physicsComputingTimeAccumulator * (1000.0/double(nbFrames)));
			totalRenderingTime += renderingTime;

			if(glfwGetTime() < 300)
			{
				averageRenderingTimeAfter2Minutes = totalRenderingTime/(double)nbComputingTimeCalculations;
			}
			
			cout << "- " << renderingTime << "ms/frame for rendering (" << averageRenderingTimeAfter2Minutes << "ms/frame on average)" << endl;
			
			nbFrames = 0;
			lastTime = currentTime;
			physicsComputingTimeAccumulator = 0;
		}

		glfwSwapBuffers(window);
		glfwPollEvents();
	}
		
	// Terminate everything
	physicsSimulator->getInstance().shutdownPhysicsSimulation();
#ifdef FREEIMAGE_LIB
	FreeImage_DeInitialise();
#endif
	glfwTerminate();

	exit(EXIT_SUCCESS);
}
