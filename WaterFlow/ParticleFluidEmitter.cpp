#include "ParticleFluidEmitter.h"
#include <iostream>
#include<vector>
//#include "Gizmos.h"
#include <GL/glew.h>

//constructor
ParticleFluidEmitter::ParticleFluidEmitter(int _maxParticles, PxVec3 _position,PxParticleFluid* _particleFluid,float _releaseDelay,GLuint* _programs){
    releaseDelay = _releaseDelay;
    maxParticles = _maxParticles;  //maximum number of particles our emitter can handle
    //allocate an array
    activeParticles = new FluidParticle[maxParticles]; //array of particle structs
	numberActiveParticles = 0;
    time = 0; //time system has been running
    respawnTime = 0; //time for next respawn
    position = _position;
    particleFluid =_particleFluid; //pointer to the physX particle system
    particleMaxAge = 8; //maximum time in seconds that a particle can live for
    //initialize the buffer
    for(int index=0;index<maxParticles;index++)
    {
        activeParticles[index].active = false;
    }

	programs = _programs;
	particlePositions = new vec4[_maxParticles];
	particleDensities = new PxF32[_maxParticles];
	particleVelocities = new vec3[_maxParticles];
	particleWeberNumbers = new PxF32[_maxParticles];
}

//destructure
ParticleFluidEmitter::~ParticleFluidEmitter()
{
    //remove all the active particles
    delete activeParticles;
}

//find the next free particle, mark it as used and return it's index.  If it can't allocate a particle: returns minus one
int ParticleFluidEmitter::getNextFreeParticle()
{
    //find particle, this is a very inefficient way to do this.  A better way would be to keep a list of free particles so we can quickly find the first free one
    for(int index=0;index<maxParticles;index++)
    {
        //when we find a particle which is free
        if(!activeParticles[index].active)
        {
            activeParticles[index].active = true; //mark it as not free
            activeParticles[index].maxTime = time+particleMaxAge;  //record when the particle was created so we know when to remove it
            return index;
        }
    }
    return -1; //returns minus if a particle was not allocated
}


//releast a particle from the system using it's index to ID it
void ParticleFluidEmitter::releaseParticle(int index)
{
    if(index >= 0 && index < maxParticles)
        activeParticles[index].active = false;
}

//returns true if a particle age is greater than it's maximum allowed age
bool ParticleFluidEmitter::tooOld(int index)
{
    if(index >= 0 && index < maxParticles && time > activeParticles[index].maxTime)
        return true;
    return false;
}

//add particles to PhysX System
bool ParticleFluidEmitter::addPhysXParticles(int particleIndex, PxU32 count)
{
    //reserve space for data
    PxParticleCreationData particleCreationData;
    //set up the data
    particleCreationData.numParticles = count;  //spawn one particle at a time,  this is inefficient and we could improve this by passing in the list of particles.
    //set up the buffers
    PxU32* myIndexBuffer = new PxU32[count];
    PxVec3* myPositionBuffer = new PxVec3[count];
    PxVec3* myVelocityBuffer = new PxVec3[count];

	for(int i = 0; i < count; i++)
	{
		myIndexBuffer[i] = particleIndex + i;
		myPositionBuffer[i] = PxVec3(position.x + (rand()%1000 - 500)/2000.0f, position.y, position.z + (rand()%1000 - 500)/2000.0f);
		myVelocityBuffer[i] = PxVec3(0, -200, 0);
	}
    
    //we can change starting position to get different emitter shapes
    particleCreationData.indexBuffer = PxStrideIterator<const PxU32>(myIndexBuffer);
    particleCreationData.positionBuffer = PxStrideIterator<const PxVec3>(myPositionBuffer);
    particleCreationData.velocityBuffer = PxStrideIterator<const PxVec3>(myVelocityBuffer);

    // create particles in *PxParticleSystem* ps
    return particleFluid->createParticles(particleCreationData);
}

//updateParticle
void ParticleFluidEmitter::upDate(float delta)
{
    //tick the emitter
    time += delta;
    respawnTime += delta;
    int numberSpawn = 0; 
    //if respawn time is greater than our release delay then we spawn at least one particle so work out how many to spawn
    if(respawnTime > releaseDelay)
    {
        numberSpawn = (int)(respawnTime/releaseDelay);
        respawnTime -= (numberSpawn * releaseDelay);

		if(numberSpawn + numberActiveParticles > maxParticles)
		{
			numberSpawn = maxParticles - numberActiveParticles;
		}
    }

	if (numberSpawn > 0) 
	{
		addPhysXParticles(numberActiveParticles, numberSpawn);
	}

    //check to see if we need to release particles because they are either too old or have hit the particle sink
	//lock the particle buffer so we can work on it and get a pointer to read data
	PxParticleReadData* rd = particleFluid->lockParticleReadData();
    // access particle data from PxParticleReadData was OK
	if (rd && rd->validParticleRange > 0)
    {
        vector<PxU32> particlesToRemove; //we need to build a list of particles to remove so we can do it all in one go
        particlesToRemove.reserve(maxParticles/10);
        PxStrideIterator<const PxVec3> positionIt(rd->positionBuffer);
		PxStrideIterator<const PxVec3> velocityIt(rd->velocityBuffer);
        PxStrideIterator<const PxParticleFlags> flagsIt(rd->flagsBuffer);
		PxStrideIterator<const PxF32>* densityIt = new PxStrideIterator<const PxF32>(((PxParticleFluidReadData*)rd)->densityBuffer);
		
		numberActiveParticles = rd->nbValidParticles;
		PxU32 bitmapRange = (rd->validParticleRange - 1) >> 5;

		// iterate over valid particle bitmap
		for (PxU32 w = 0; w <= bitmapRange; ++w)
		{
			for(PxU32 b = rd->validParticleBitmap[w]; b > 0; b &= b-1)
			{
				PxU32 index = (w << 5 | lowestSetBit(b));
				bool shouldBeRemoved = false;

				// check if particle is drained or dead
				if (flagsIt[index] & PxParticleFlag::eCOLLISION_WITH_DRAIN || flagsIt[index] & PxParticleFlag::eSPATIAL_DATA_STRUCTURE_OVERFLOW)
				{
					//mark our local copy of the particle free
                    releaseParticle(index);
                    //add to our list of particles to remove
                    particlesToRemove.push_back(index);
					shouldBeRemoved = true;
				}

				if (shouldBeRemoved) continue;
				if (positionIt.ptr()) particlePositions[index] = vec4(positionIt[index].x, positionIt[index].y, positionIt[index].z, 1.0f);
				if (velocityIt.ptr()) particleVelocities[index] = vec3(velocityIt[index].x, velocityIt[index].y, velocityIt[index].z);
				if (densityIt) particleDensities[index] = (*densityIt)[index];

				float localLiquidCharacteristicDimension = 0.000001;
				float surfaceTensionCoefficient = 0.0728;
				particleWeberNumbers[index] = (particleDensities[index] * pow(length(particleVelocities[index]), 2) * localLiquidCharacteristicDimension)/surfaceTensionCoefficient;
			}
		}

		if (densityIt) delete densityIt;

        // return ownership of the buffers back to the SDK
        rd->unlock();

        //if we have particles to release then pass the particles to remove to PhysX so it can release them
        if(particlesToRemove.size()>0)
        {
            //create a buffer of particle indicies which we are going to remove
            PxStrideIterator<const PxU32> indexBuffer(&particlesToRemove[0]);
            //free particles from the physics system
            particleFluid->releaseParticles(particlesToRemove.size(), indexBuffer);
        }
    }
}

int ParticleFluidEmitter::lowestSetBit(PxU32 v)
{
	static const int MultiplyDeBruijnBitPosition[32] = { 0, 1, 28, 2, 29, 14, 24, 3, 30, 22, 20, 15, 25, 17, 4, 8, 31, 27, 13, 23, 21, 19, 16, 7, 26, 12, 18, 6, 11, 5, 10, 9 };
	return MultiplyDeBruijnBitPosition[((uint32_t)((v & -v) * 0x077CB531U)) >> 27];
}