#include "ShaderReader.h"

ShaderReader ShaderReader::instance = ShaderReader();

ShaderReader::ShaderReader()
{
}

ShaderReader::ShaderReader(const ShaderReader& shaderReader)
{
}

ShaderReader::~ShaderReader()
{
}

ShaderReader &ShaderReader::getInstance()
{
	return instance;
}

string ShaderReader::readShaderSource(const char* shaderFile)
{
	ifstream ifile(shaderFile);
	string filetext;

	while (ifile.good())
	{
		string line;
		getline(ifile, line);
		filetext.append(line + "\n");
	}

	return filetext;
}

GLuint ShaderReader::LoadShader(const char* vShaderFile, const char* fShaderFile)
{
    struct Shader {
        const char*		filename;
        GLenum			type;
        const GLchar*	source;
		GLint			length;
    }  shaders[2] = {
        { vShaderFile, GL_VERTEX_SHADER, NULL },
        { fShaderFile, GL_FRAGMENT_SHADER, NULL }
    };
    
    GLuint program = glCreateProgram();
    
    for ( int i = 0; i < 2; ++i ) {
        Shader& s = shaders[i];
		string shadersource = readShaderSource( s.filename );
		s.source = shadersource.c_str();
		s.length = shadersource.size();
        if ( shaders[i].source == NULL ) {
            std::cout << "Failed to read " << s.filename << std::endl;
            exit( EXIT_FAILURE );
        }
        
        GLuint shader = glCreateShader( s.type );
		glShaderSource( shader, 1, (const GLchar**) &s.source, &s.length );
        glCompileShader( shader );
        
        GLint  compiled;
        glGetShaderiv( shader, GL_COMPILE_STATUS, &compiled );
        if ( !compiled ) {
            std::cout << s.filename << " failed to compile:" << std::endl;
            GLint  logSize;
            glGetShaderiv( shader, GL_INFO_LOG_LENGTH, &logSize );
            char* logMsg = new char[logSize];
            glGetShaderInfoLog( shader, logSize, NULL, logMsg );
            std::cout << logMsg << std::endl;
            delete [] logMsg;
            
            exit( EXIT_FAILURE );
        }
        
        glAttachShader( program, shader );
    }
    
    /* link  and error check */
    glLinkProgram(program);
    
    GLint  linked;
    glGetProgramiv( program, GL_LINK_STATUS, &linked );
    if ( !linked ) {
        std::cout << "Shader program failed to link" << std::endl;
        GLint  logSize;
        glGetProgramiv( program, GL_INFO_LOG_LENGTH, &logSize);
        char* logMsg = new char[logSize];
        glGetProgramInfoLog( program, logSize, NULL, logMsg );
        std::cout << logMsg << std::endl;
        delete [] logMsg;
        
        exit( EXIT_FAILURE );
    }
    
    return program;
}
