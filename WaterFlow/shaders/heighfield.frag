#version 430

in vec3 Position;
in vec3 Normal;
in vec2 TexCoords;
in float Height;

uniform sampler2D GrassTexture;
uniform sampler2D GroundTexture;
uniform vec4 LightPosition;
uniform vec3 LightIntensity;
uniform bool PhotoRealistic;

out vec4 fColor;

vec4 shade(vec3 normal, vec3 pos, vec4 color)
{
	vec3 s = normalize(LightPosition.xyz - pos);
	float cosine = max(0.0f, dot(s, normal));
	vec4 ambient = color * 0.5f;
	vec4 diffuse = color * cosine;

	return vec4(LightIntensity * (ambient.rgb + diffuse.rgb), 1.0);
}

vec4 toonShade(vec3 normal, vec3 pos, vec4 color)
{
	vec3 s = normalize(LightPosition.xyz - pos);
	float cosine = max(0.0f, dot(s, normal));
	vec4 ambient = color * 0.5f;
	vec4 diffuse = color * floor(cosine * 3.0f) * (1.0f / 3.0f);

	return vec4(LightIntensity * (ambient.rgb + diffuse.rgb), 1.0);
}

void main()
{
	const float fRange1 = 0.0f;
	const float fRange2 = 100.0f;
	float mixFactor = clamp((Height - fRange1)/(fRange2 - fRange1), 0.0f, 1.0f);

	fColor = PhotoRealistic ? 
		shade(Normal, Position, mix(texture(GroundTexture, TexCoords), texture(GrassTexture, TexCoords), mixFactor)) : 
		toonShade(Normal, Position, mix(texture(GroundTexture, TexCoords), texture(GrassTexture, TexCoords), mixFactor));
}
