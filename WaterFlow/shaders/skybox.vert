#version 430

layout (location = 0) in vec3 vPosition;

out vec3 TexCoords;

uniform mat4 View, Projection;

void main()
{
	gl_Position = Projection * View * vec4(vPosition, 1.0);  
    TexCoords = vPosition;
}
