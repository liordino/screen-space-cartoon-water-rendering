#version 430

in vec2 textureCoords;

out vec4 particleDepthStencil;

uniform float Near, Far;

// Textures
uniform sampler2D ParticlesSmoothedDepthTexture;
uniform sampler2D BackgroundDepthTexture;

void main()
{
	vec4 particleDepth = texture(ParticlesSmoothedDepthTexture, textureCoords);
	float backgroundDepth = (2.0f * Near) / (Far + Near - (texture(BackgroundDepthTexture, textureCoords).r) * (Far - Near));
	
	if (particleDepth.r <= backgroundDepth)
	{
		discard;
	}
	
	particleDepthStencil = particleDepth;
}
