#version 430

in vec3 TexCoords;

out vec4 fColor;

uniform samplerCube Skybox;

void main()
{    
    fColor = texture(Skybox, TexCoords);
}
