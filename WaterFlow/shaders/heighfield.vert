#version 430

layout (location = 0) in vec4 vPosition;
layout (location = 1) in vec3 vNormal;
layout (location = 2) in vec2 vTexCoords;
layout (location = 3) in float vHeight;

out vec3 Position;
out vec3 Normal;
out vec2 TexCoords;
out float Height;

uniform mat4 Model, View, Projection;

void main()
{
	mat4 ModelView = View * Model;
	mat4 ModelViewProjection = Projection * ModelView;

	Normal = normalize(vec3(ModelView * vec4(vNormal, 0.0)));
	Position = vec3(ModelView * vPosition);
	TexCoords = vTexCoords;
	Height = vHeight;

	gl_Position = ModelViewProjection * vPosition;
}
