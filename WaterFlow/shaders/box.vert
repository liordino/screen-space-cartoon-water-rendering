#version 430

layout (location = 0) in vec4 vPosition;
layout (location = 1) in vec3 vNormal;

out vec3 Position;
out vec3 Normal;
out vec3 TexCoords;

uniform mat4 Model, View, Projection;

void main()
{
	mat4 ModelView = View * Model;
	mat4 ModelViewProjection = Projection * ModelView;

	Position = vec3(ModelView * vPosition);
	Normal = normalize(vec3(ModelView * vec4(vNormal, 0.0)));
	TexCoords = vPosition.xyz;

	gl_Position = ModelViewProjection * vPosition;
}
