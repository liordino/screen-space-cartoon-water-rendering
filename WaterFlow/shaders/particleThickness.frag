#version 430

out vec4 particleThickness;

void main()
{
	vec3 normal;

	normal.xy = (gl_PointCoord - 0.5f) * 2.0f;
	float dist = length(normal);

	// Discard pixels outside the sphere
	if(dist > 1.0f) 
	{
		discard;
	}
	
	// Set up thickness
	/* Method 1 */
	// particleThickness = (1.0f - dist)/10.0f;

	/* Method 2 - Simple gaussian distribution */
	float sigma = 3.0;
	float mu = 0.0;
	
	particleThickness = vec4(vec3(0.02 * exp(-(dist-mu)*(dist-mu)/(2.0*sigma))), 1.0);
}
