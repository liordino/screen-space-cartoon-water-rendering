#version 430

in vec2 textureCoords;

out vec4 smoothedParticleDepth;

uniform sampler2D DepthTexture;
uniform vec2 ScreenResolution;
uniform mat4 Projection;
uniform float Fovy;
uniform float Epsilon;

// Mean curvature. From "Screen Space Fluid Rendering with Curvature Flow"
vec3 meanCurvature(vec2 pos) {
	// Samples for finite differencing
	float depth = texture(DepthTexture, pos).r;
	float depth_d = depth;
	float depth_l = depth;
	float depth_r = depth;
	float depth_u = depth;

	if(pos.y - (1.0/ScreenResolution.y) >= 0.0) depth_d = texture(DepthTexture, pos + (vec2( 0.0, -1.0)/ScreenResolution)).r;
	if(pos.x - (1.0/ScreenResolution.x) >= 0.0) depth_l = texture(DepthTexture, pos + (vec2(-1.0,  0.0)/ScreenResolution)).r;
	if(pos.x + (1.0/ScreenResolution.x) < ScreenResolution.x) depth_r = texture(DepthTexture, pos + (vec2( 1.0,  0.0)/ScreenResolution)).r;
	if(pos.y + (1.0/ScreenResolution.y) < ScreenResolution.y) depth_u = texture(DepthTexture, pos + (vec2( 0.0,  1.0)/ScreenResolution)).r;
	
	if(abs(depth_d - depth) > 10.0) depth_d = depth;
	if(abs(depth_l - depth) > 10.0) depth_l = depth;
	if(abs(depth_r - depth) > 10.0) depth_r = depth;
	if(abs(depth_u - depth) > 10.0) depth_u = depth;

	// derivatives (finite differencing)
	float dx = (0.5 * (depth_r - depth_l));
	float dy = (0.5 * (depth_u - depth_d));

	// second derivatives 
	float dxx = (depth_l - 2.0 * depth + depth_r);
	float dyy = (depth_d - 2.0 * depth + depth_u);

	// Projection transform inversion terms
	float fov = Fovy * (ScreenResolution.x/ScreenResolution.y);
	float cx = 2.0 / (ScreenResolution.x * tan(fov/2.0));
	float cy = 2.0 / (ScreenResolution.y * tan(fov/2.0));

	// constants
	const float dx2 = dx*dx; 
	const float dy2 = dy*dy;
	const float cx2 = cx*cx;
	const float cy2 = cy*cy;

	// calculate curvature
	float D = cy2*dx2 + cx2*dy2 + cx2+cy2*depth*depth;
	float H = cy*dxx*D - cy*dx*(cy2*dx*dxx + cx2*cy2*depth*dx) + cx*dyy*D - cx*dy*(cx2*dy*dyy + cx2*cy2*depth*dy);
	H /= pow(D, 3.0/2.0);

	// curvature dependent shift
	return vec3(depth + Epsilon * H);
}

// Mean curvature. From "Screen Space Fluid Rendering with Curvature Flow"
vec3 meanCurvature2(vec2 pos) {
	// Width of one pixel
	vec2 dx = vec2(1.0f / ScreenResolution.x, 0.0f);
	vec2 dy = vec2(0.0f, 1.0f / ScreenResolution.y);

	// Central z value
	float zc =  texture(DepthTexture, pos);

	// Take finite differences
	// Central differences give better results than one-sided here.
	// TODO better boundary conditions, possibly.
	// Remark: This is not easy, get to choose between bad oblique view smoothing or merging of unrelated particles
	float zdxp = texture(DepthTexture, pos + dx);
	float zdxn = texture(DepthTexture, pos - dx);
	float zdx = 0.5f * (zdxp - zdxn);
	zdx = (zdxp == 0.0f || zdxn == 0.0f) ? 0.0f : zdx;

	float zdyp = texture(DepthTexture, pos + dy);
	float zdyn = texture(DepthTexture, pos - dy);
	float zdy = 0.5f * (zdyp - zdyn);
	zdy = (zdyp == 0.0f || zdyn == 0.0f) ? 0.0f : zdy;

	// Take second order finite differences
	float zdx2 = zdxp + zdxn - 2.0f * zc;
	float zdy2 = zdyp + zdyn - 2.0f * zc;

	// Second order finite differences, alternating variables
	float zdxpyp = texture(DepthTexture, pos + dx + dy);
	float zdxnyn = texture(DepthTexture, pos - dx - dy);
	float zdxpyn = texture(DepthTexture, pos + dx - dy);
	float zdxnyp = texture(DepthTexture, pos - dx + dy);
	float zdxy = (zdxpyp + zdxnyn - zdxpyn - zdxnyp) / 4.0f;

	// Projection transform inversion terms
	float cx = 2.0f / (ScreenResolution.x * -Projection[0][0]);
	float cy = 2.0f / (ScreenResolution.y * -Projection[1][1]);

	// Normalization term
	float d = cy * cy * zdx * zdx + cx * cx * zdy * zdy + cx * cx * cy * cy * zc * zc;
		
	// Derivatives of said term
	float ddx = cy * cy * 2.0f * zdx * zdx2 + cx * cx * 2.0f * zdy * zdxy + cx * cx * cy * cy * 2.0f * zc * zdx;
	float ddy = cy * cy * 2.0f * zdx * zdxy + cx * cx * 2.0f * zdy * zdy2 + cx * cx * cy * cy * 2.0f * zc * zdy;

	// Temporary variables to calculate mean curvature
	float ex = 0.5f * zdx * ddx - zdx2 * d;
	float ey = 0.5f * zdy * ddy - zdy2 * d;

	// Finally, mean curvature
	float h = 0.5f * ((cy * ex + cx * ey) / pow(d, 1.5f));
	
	// Vary contribution with absolute depth differential - trick from pySPH
	const float dt = 0.000055f;
	const float dzt = 1000.0f;

	return vec3(zc + h * dt * (1.0f + (abs(zdx) + abs(zdy)) * dzt));
}

// Mean curvature. From "Screen Space Fluid Rendering with Curvature Flow"
vec3 meanCurvature3(vec2 pos) {
	float fov = Fovy * (ScreenResolution.x/ScreenResolution.y);
	vec2 u_cxy = vec2(2.0f/(ScreenResolution.x * tan(fov*0.5*3.14159265/180.0)),  2.0f/(ScreenResolution.y * tan(fov*0.5*3.14159265/180.0)));
	vec2 u_cxy2 = vec2 (u_cxy.x * u_cxy.x, u_cxy.y * u_cxy.y);

	//Perform screen-space curvature flow filtering
    float depth = texture(DepthTexture, pos).r;
	float depth_d = depth;
	float depth_l = depth;
	float depth_r = depth;
	float depth_u = depth;

	if(pos.y - (1.0/ScreenResolution.y) >= 0.0) depth_d = texture(DepthTexture, pos + (vec2( 0.0, -1.0)/ScreenResolution)).r;
	if(pos.x - (1.0/ScreenResolution.x) >= 0.0) depth_l = texture(DepthTexture, pos + (vec2(-1.0,  0.0)/ScreenResolution)).r;
	if(pos.x + (1.0/ScreenResolution.x) < ScreenResolution.x) depth_r = texture(DepthTexture, pos + (vec2( 1.0,  0.0)/ScreenResolution)).r;
	if(pos.y + (1.0/ScreenResolution.y) < ScreenResolution.y) depth_u = texture(DepthTexture, pos + (vec2( 0.0,  1.0)/ScreenResolution)).r;

    if( abs( depth_r - depth ) > 10 ) depth_r = depth;	
    if( abs( depth_l - depth ) > 10 ) depth_l = depth;	
    if( abs( depth_d - depth ) > 10 ) depth_d = depth;	
    if( abs( depth_u - depth ) > 10 ) depth_u = depth;	

	//derivates 
	float dx = 0.5 * ( depth_r - depth_l );
	float dy = 0.5 * ( depth_u - depth_d );

	//second derivatives
	float dxx = ( depth_l - 2.0 * depth + depth_r );
	float dyy = ( depth_d - 2.0 * depth + depth_u );

	float D = u_cxy2.y * dx * dx + u_cxy2.x * dy * dy + u_cxy2.x*u_cxy2.y*depth*depth;
	float H = u_cxy.y * dxx * D - u_cxy.y*dx*( u_cxy2.y * dx * dxx + u_cxy2.x * u_cxy2.y * depth * dx ) +
	          u_cxy.x * dyy * D - u_cxy.x*dy*( u_cxy2.x * dy * dyy + u_cxy2.x *u_cxy2.y * depth * dy );
	H /= pow( D, 1.5 );

	depth = depth + Epsilon * H;

	return vec3(depth);
}

void main() {
	vec4 particleDepth = texture(DepthTexture, textureCoords);

	if (particleDepth == 0.0f) 
	{
		discard;
	}

	smoothedParticleDepth = vec4(meanCurvature3(textureCoords), particleDepth.a);
	gl_FragDepth = smoothedParticleDepth.x;
}	
