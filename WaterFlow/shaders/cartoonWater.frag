#version 430
#extension GL_NV_shadow_samplers_cube : enable

in vec2 textureCoords;

out vec4 fColor;

uniform mat4 ModelView, Projection;
uniform float Near, Far;
uniform vec2 ScreenResolution;
uniform vec4 LightDir;
uniform vec3 LightIntensity;
uniform int CartoonLevels;
uniform vec4 WaterColor;
uniform bool PhotoRealistic;
uniform int OpticalEffects;
const vec4 SpecularColor = vec4(0.5, 0.5, 0.5, 1.0);
const float shininess = 40.0;
const vec4 RealisticWaterColor = vec4(0.2, 0.5, 0.7, 1.0);

// Reflection/Refraction interpolation
const float Xi = 0.4;
const float X = 0.7;
const float deltaX = X - Xi;
const float deltaX3 = deltaX * deltaX * deltaX;
const float refrMin = 0.40;
const float refrMax = 1.00;
const float reflMin = 0.00;
const float reflMax = 0.60;

// Textures
uniform sampler2D BackgroundTexture;
uniform sampler2D BackgroundDepthTexture;
uniform sampler2D ParticlesSmoothedDepthTexture;
uniform sampler2D ParticlesDepthStencilTexture;
uniform sampler2D FoamDepthTexture;
uniform sampler2D FoamDepthStencilTexture;
uniform sampler2D ParticlesSmoothedThicknessTexture;
uniform samplerCube EnvironmentTexture;

// Reflection/refraction interpolation
float interpolate(float kmin, float kmax, vec3 n, vec3 v)
{
	float x;
	float dotVN = dot(v, n);

	if (dotVN < 0.0)
	{
		x = dotVN * -1.0;
	}
	else
	{
		x = dotVN;
	}

	if (x < Xi) return kmin;
	if (x > X) return kmax;

	float deltak = kmax - kmin;

	float a = - (2.0 * deltak) / deltaX3;
	float b = (3.0 * deltak * (X + Xi)) / deltaX3;
	float c = - (6.0 * deltak * X * Xi) / deltaX3;
	float d = kmin + (deltak * Xi * Xi * (3.0 * X - Xi)) / deltaX3;

	return ((a*x+b)*x+c)*x+d;
}

// Fresnel coefficient. Obtained from github.com/halcy/simpleflow
float fresnel(float rr1, float rr2, vec3 n, vec3 d)
{
	float r = rr1 / rr2;
	float theta1 = dot(n, -d);
	float theta2 = sqrt(1.0 - r * r * (1.0 - theta1 * theta1));

	// Figure out what the Fresnel equations say about what happens next
	float rs = (rr1 * theta1 - rr2 * theta2) / (rr1 * theta1 + rr2 * theta2);
	rs = rs * rs;
	float rp = (rr1 * theta2 - rr2 * theta1) / (rr1 * theta2 + rr2 * theta1);
	rp = rp * rp;

	return ((rs + rp) / 2.0);
}

// Obtained from github.com/halcy/simpleflow
vec2 sphereMap(vec3 dir)
{
	float m = 2.0 * sqrt(dir.x * dir.x + dir.y * dir.y + (dir.z + 1.0) * (dir.z + 1.0));
	
	return vec2(dir.x / m + 0.5, dir.y / m + 0.5);
}

// Compute eye-space position. Obtained from github.com/halcy/simpleflow
vec3 eyeSpacePos(vec2 pos)
{
	float depth = texture(ParticlesSmoothedDepthTexture, pos).x;
	pos = (pos - vec2(0.5)) * 2.0;

	return (depth * vec3(-pos.x * Projection[0][0], -pos.y * Projection[1][1], 1.0));
}

// Compute eye-space normal. Obtained from github.com/halcy/simpleflow
vec3 eyeSpaceNormal(vec2 pos)
{
	// Width o one pixel
	vec2 dx = vec2(1.0 / ScreenResolution.x, 0.0);
	vec2 dy = vec2(0.0, 1.0 / ScreenResolution.y);

	// Central z
	float zc = texture(ParticlesSmoothedDepthTexture, pos).x;

	// Derivatives o z
	// For shading, one-sided only-the-one-that-works version
	float zdxp = texture(ParticlesSmoothedDepthTexture, pos + dx).x;
	float zdxn = texture(ParticlesSmoothedDepthTexture, pos - dx).x;
	float zdx = (zdxp == 0.0) ? (zdxn == 0.0 ? 0.0 : (zc - zdxn)) : (zdxp - zc);

	float zdyp = texture(ParticlesSmoothedDepthTexture, pos + dy).x;
	float zdyn = texture(ParticlesSmoothedDepthTexture, pos - dy).x;
	float zdy = (zdyp == 0.0) ? (zdyn == 0.0 ? 0.0 : (zc - zdyn)) : (zdyp - zc);

	// Projection inversion
	float cx = 2.0 / (ScreenResolution.x * -Projection[0][0]);
	float cy = 2.0 / (ScreenResolution.y * -Projection[1][1]);

	// Screenspace coordinates
	float sx = floor(pos.x * (ScreenResolution.x - 1.0));
	float sy = floor(pos.y * (ScreenResolution.y - 1.0));
	float wx = (ScreenResolution.x - 2.0 * sx) / (ScreenResolution.x * Projection[0][0]);
	float wy = (ScreenResolution.y - 2.0 * sy) / (ScreenResolution.y * Projection[1][1]);

	// Eyespace position derivatives   
	vec3 pdx = normalize(vec3(cx * zc + wx * zdx, wy * zdx, zdx));
	vec3 pdy = normalize(vec3(wx * zdy, cy * zc + wy * zdy, zdy));

	return normalize(cross(pdx, pdy));
}

vec4 toonShade(vec3 normal, vec3 pos, vec4 color)
{
	vec3 s = normalize(LightDir.xyz - pos);
	float cosine = max(0.0, dot(s, normal));
	vec4 ambient = WaterColor/2.0;
	vec4 diffuse = color * floor(cosine * CartoonLevels) * (1.0 / CartoonLevels);
	
	return vec4(LightIntensity, 1.0) * (ambient + diffuse);
}

void main()
{
	vec4 particleDepth = texture(ParticlesSmoothedDepthTexture, textureCoords);
	vec4 particleDepthStencil = texture(ParticlesDepthStencilTexture, textureCoords);
	vec4 foamDepth = texture(FoamDepthTexture, textureCoords);
	vec4 foamDepthStencil = texture(FoamDepthStencilTexture, textureCoords);
	vec4 background = texture(BackgroundTexture, textureCoords);
	float backgroundDepth = (2.0 * Near) / (Far + Near - (texture(BackgroundDepthTexture, textureCoords).r) * (Far - Near));

	if ((particleDepth.r == 0.0 || (particleDepth.r != 0.0 && particleDepthStencil.r != 0.0))
		&& (foamDepth.r == 0.0 || (foamDepth.r != 0.0 && foamDepthStencil.r != 0.0)))
	{
		fColor = background;
		gl_FragDepth = backgroundDepth;
	}
	else
	{
		float thickness = texture(ParticlesSmoothedThicknessTexture, textureCoords).x * 0.075; // Lower thickness values make the refraction look smoother
		vec3 normal = eyeSpaceNormal(textureCoords);
		vec3 eyePos = eyeSpacePos(textureCoords);
		eyePos = (vec4(eyePos, 1.0) * inverse(ModelView)).xyz;
		vec3 incident = normalize(LightDir.xyz);
		vec3 fromEye = normalize(eyePos);
		vec3 reflectedEye = normalize(reflect(fromEye, normal));
		vec4 sceneCol = texture(BackgroundTexture, textureCoords + (normal.xy * thickness));
		vec4 environmentColor = textureCube(EnvironmentTexture, reflectedEye);

		if (PhotoRealistic)
		{
			float lambert = max(0.0, dot(normalize(LightDir.xyz), normal));
			float cosine = max(0.0, dot(normalize(LightDir.xyz - eyePos), normal));
			vec4 ambient = RealisticWaterColor;
			vec4 diffuse = RealisticWaterColor * cosine;
			float specular = clamp(fresnel(0.25, 0.5, normal, fromEye), 0.0, 0.2);

			vec4 absorbColor = (vec4(LightIntensity, 1.0) * (ambient + diffuse)) * exp(-thickness);
			vec4 foamColor = vec4(1.0);

			fColor = mix(mix((lambert + 0.2) * absorbColor * (1.0 - specular) + specular * environmentColor, foamColor, foamDepth.a), sceneCol, 0.5);
		}
		else
		{
			float refractionFactor = interpolate(refrMin, refrMax, normal, reflectedEye);
			float reflectionFactor = 1.0 - refractionFactor;

			vec4 absorbColor = toonShade(normal, eyePos, WaterColor);
			vec4 refractedColor = sceneCol * exp(-thickness) * refractionFactor;
			vec4 reflectedColor = environmentColor * reflectionFactor;
			vec4 foamColor = vec4(1.0);

			if (OpticalEffects == 0) // No optical effects
			{
				fColor = mix(absorbColor, foamColor, foamDepth.a*1.5);
			}
			else if (OpticalEffects == 1) // Only refraction
			{
				fColor = mix(mix(absorbColor, foamColor, foamDepth.a*1.5), refractedColor, 0.35);
			}
			else if (OpticalEffects == 2) // Only reflection
			{
				fColor = mix(mix(absorbColor, foamColor, foamDepth.a*1.5), reflectedColor, 0.35);
			}
			else if (OpticalEffects == 3) // Everything
			{
				fColor = mix(mix(absorbColor, foamColor, foamDepth.a*1.5), refractedColor + reflectedColor, 0.35);
			}
		}

		gl_FragDepth = particleDepth.r;
	}
}
