#version 430

in vec2 textureCoords;

out vec4 particleEdges;

uniform float Near, Far;
uniform vec2 ScreenResolution;
uniform sampler2D ParticlesSmoothedDepthTexture;
uniform sampler2D BackgroundDepthTexture;

const float SigmaE = 1.0;
const float SigmaR = 1.6;
const float Tau = 0.99;
const float Phi = 2.0;

void main() {
	vec4 particleDepth = texture(ParticlesSmoothedDepthTexture, textureCoords);
	float backgroundDepth = (2.0f * Near) / (Far + Near - (texture(BackgroundDepthTexture, textureCoords).r) * (Far - Near));
	
	float twoSigmaESquared = 2.0 * SigmaE * SigmaE;
	float twoSigmaRSquared = 2.0 * SigmaR * SigmaR;
	int halfWidth = int(ceil( 2.0 * SigmaR ));

	vec2 sum = vec2(0.0);
	vec2 norm = vec2(0.0);

	for ( int i = -halfWidth; i <= halfWidth; ++i ) {
		for ( int j = -halfWidth; j <= halfWidth; ++j ) {
			float d = length(vec2(i,j));
			vec2 kernel = vec2( exp( -d * d / twoSigmaESquared ), exp( -d * d / twoSigmaRSquared ));
                
			vec2 L = texture2D(ParticlesSmoothedDepthTexture, textureCoords + (vec2(i,j) / ScreenResolution)).xx;

			norm += 2.0 * kernel;
			sum += kernel * L;
		}
	}

	sum /= norm;

	float H = 100.0 * (sum.x - Tau * sum.y);
	float edges = ( H > 0.0 ) ? 1.0 : 2.0 * smoothstep(-2.0, 2.0, Phi * H );

	particleEdges = vec4(vec3(edges), 1.0);
}
