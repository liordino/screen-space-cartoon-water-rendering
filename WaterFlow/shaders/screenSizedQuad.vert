#version 430

layout (location = 0) in vec4 vPosition;

out vec2 textureCoords;

void main()
{
	textureCoords = vPosition.xy * vec2(0.5f) + vec2(0.5f);
	gl_Position = vPosition;
}
