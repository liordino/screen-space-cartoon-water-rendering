#version 430

in vec4 eyeSpacePos;
in float eyeSpaceRadius, weberNumber;

out vec4 particleDepth;

uniform mat4 Projection;
uniform vec2 ScreenResolution;
uniform float Near, Far, DropletThreshold;

void main()
{
	if (weberNumber >= DropletThreshold)
	{
		discard;
	}

	vec3 normal;

	normal.xy = (gl_PointCoord - 0.5f) * 2.0f;
	float dist = length(normal);
	float sizeAndAlphaFactor = clamp((weberNumber)/(DropletThreshold), 0.0f, 1.0f);

	// Discard pixels outside the sphere
	if(dist > 1.0f * sizeAndAlphaFactor) 
	{
		discard;
	}

	// Set up rest of normal
	normal.z = sqrt(1.0f - dist);
	normal.y = -normal.y;
	normal = normalize(normal);

	// Calculate fragment position in eye space, project to find depth
	vec4 fragPos = vec4(eyeSpacePos.xyz + normal * eyeSpaceRadius / ScreenResolution.y, 1.0);
	vec4 clipspacePos = Projection * fragPos;

	// Set up output
	float deviceDepth = clipspacePos.z / clipspacePos.w;
	float fragDepth = ((2.0f * Near) / (Far + Near - (deviceDepth * 0.5 + 0.5) * (Far - Near)));
	
	particleDepth = vec4(vec3(fragDepth), 1.0f - sizeAndAlphaFactor);
	gl_FragDepth = fragDepth;
}
