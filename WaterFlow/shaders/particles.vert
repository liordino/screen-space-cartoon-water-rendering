#version 430

layout (location = 0) in vec4 vPosition;
layout (location = 1) in float vWeberNumber;

out vec4 eyeSpacePos;
out float eyeSpaceRadius, velocity, weberNumber;

uniform mat4 ModelView, Projection;
uniform vec2 ScreenResolution;
uniform float ParticleSizeFactor;

void main()
{
    // Transform
	eyeSpacePos = ModelView * vPosition;
	eyeSpaceRadius = 1.0f / (-eyeSpacePos.z * ParticleSizeFactor * (1.0f / ScreenResolution.y));
	vec4 clipSpacePos = Projection * eyeSpacePos;

	// Set up variables for rasterizer
	gl_Position = clipSpacePos;
	gl_PointSize = eyeSpaceRadius;

	// Send velocity to fragment shader
	velocity = vPosition.w;
	weberNumber = vWeberNumber;
}
