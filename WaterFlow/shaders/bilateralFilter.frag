#version 430

in vec2 textureCoords;

out vec4 smoothedParticleDepth;

uniform sampler2D DepthTexture;
uniform vec2 Direction;
uniform vec2 ScreenResolution;

uniform float Kernel[50];
uniform float DomainSigma;
uniform int KernelCenter;

float normpdf(in float x, in float sigma)
{
	return 0.39894f*exp(-0.5f*x*x/(sigma*sigma))/sigma;
}

void main()
{
	vec4 fragmentOriginalValue = texture(DepthTexture, textureCoords);

	if (fragmentOriginalValue.r == 0.0f)
	{
		discard;
	}

	float fragmentFinalValue = 0.0f;
	float z = 0.0f;
	float currentValue;
	float smoothingFactor;
	float bZ = 1.0/normpdf(0.0, DomainSigma);

	if(Direction == vec2(1.0))
	{
		for(int i = -KernelCenter; i <= KernelCenter; ++i)
		{
			for(int j = -KernelCenter; j <= KernelCenter; ++j)
			{
				currentValue = texture(DepthTexture, (textureCoords+(vec2(float(i), float(j))/ScreenResolution))).r;
				smoothingFactor = normpdf(currentValue - fragmentOriginalValue.r, DomainSigma)*bZ*Kernel[KernelCenter+i]*Kernel[KernelCenter+j];
				z += smoothingFactor;
				fragmentFinalValue += smoothingFactor*currentValue;
			}
		}
	}
	else
	{
		for(int i = -KernelCenter; i <= KernelCenter; ++i)
		{
			currentValue = texture(DepthTexture, (textureCoords+(vec2(float(i))/ScreenResolution) * Direction)).r;
			smoothingFactor = normpdf(currentValue - fragmentOriginalValue.r, DomainSigma)*bZ*Kernel[KernelCenter+i];
			z += smoothingFactor;
			fragmentFinalValue += smoothingFactor*currentValue;
		}
	}

	float fragmentDepth = fragmentFinalValue/z;

	smoothedParticleDepth = vec4(vec3(fragmentDepth), fragmentOriginalValue.a);
	gl_FragDepth = fragmentDepth;
}
