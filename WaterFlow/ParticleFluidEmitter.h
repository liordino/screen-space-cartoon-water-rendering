#pragma once
#include <PhysX\PxPhysicsAPI.h>
#include <glm\ext.hpp>
#include <GL\glew.h>

// Shader programs
#define SHADER_DEPTH					0
#define SHADER_FOAM_DEPTH				1
#define SHADER_THICKNESS				2
#define SHADER_DEPTH_STENCIL			3
#define SHADER_EDGES					4
#define SHADER_BILATERAL_FILTER			5
#define SHADER_CURVATURE_FLOW_FILTER	6
#define SHADER_CARTOON_WATER			7
#define SHADER_SKYBOX					8
#define SHADER_PARTICLE_CLOUD			9

using namespace physx;
using namespace std;
using namespace glm;

//simple struct for our particles

struct FluidParticle
{
    bool active;
    float maxTime;
};


//simple class for particle emitter.  For a real system we would make this a base class and derive different emitters from it by making functions virtual and overloading them.
class ParticleFluidEmitter
{
private:
	PxCudaContextManager* mCudaContextManager;
    int maxParticles;
    FluidParticle *activeParticles;
    float releaseDelay;
    int getNextFreeParticle();
    bool addPhysXParticle(int particleIndex);
    bool addPhysXParticles(int particleIndex, PxU32 count);
	int ParticleFluidEmitter::lowestSetBit(PxU32 v);
    float time;
    float respawnTime;
    float particleMaxAge;
    PxVec3 position;
    int boxWidth;
    int boxHeight;
public:
    PxParticleFluid* particleFluid;
    ParticleFluidEmitter(int _maxParticles,PxVec3 _position,PxParticleFluid* _pf,float _releaseDelay,GLuint* _programs);
    ~ParticleFluidEmitter();
    void upDate(float delta);
    void releaseParticle(int);
    bool tooOld(int);
    void renderParticles();
	int numberActiveParticles;
    int rows;
    int cols;
    int depth;
	GLuint* programs;
	vec4* particlePositions;
	PxF32* particleDensities;
	vec3* particleVelocities;
	PxF32* particleWeberNumbers;
};
