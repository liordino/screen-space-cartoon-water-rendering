#include <iostream> // TODO: usado para debug, remover quando tirar os debugs
#include <fstream>
#include <string>

#include <GL\glew.h>

using namespace std;

class ShaderReader
{
public:
	static ShaderReader &getInstance();

	string ShaderReader::readShaderSource(const char* shaderFile);
	GLuint ShaderReader::LoadShader(const char* vShaderFile, const char* fShaderFile);
	
	virtual ~ShaderReader(void);
private:	
	ShaderReader();
	ShaderReader(const ShaderReader& shaderReader);

	static ShaderReader instance;
};
