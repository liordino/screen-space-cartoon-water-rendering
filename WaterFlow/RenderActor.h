#include <PhysX\PxPhysicsAPI.h>
#include <PhysX\pxtask\PxTask.h>
#include <GL\glew.h>
#include <glm\glm.hpp>
#include <glm\gtc\matrix_transform.hpp>
#include <glm\gtc\type_ptr.hpp>
#include <glm\gtx\fast_square_root.hpp>
#include <glm\gtc\quaternion.hpp>

using namespace glm;
using namespace physx;

class RenderActor
{
public:
	// Vertices, its normals and UVs
	vec4* vertices;
	vec3* normals;
	vec2* uvs;
	float* heights;
	uint32 verticesCount;

	// Indices
	uint32* indices;
	uint32 indicesCount;

	// Rendering properties
	PxActor* renderActor; 
	GLuint* programs;
	GLuint VAO, VBO, EBO;
};