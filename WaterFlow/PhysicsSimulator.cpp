//
//  PhysicsSimulator.cpp
//  WaterFlowMac
//
//  Created by liord_000 Neto on 14/08/14.
//  Copyright (c) 2014 liord_000 Neto. All rights reserved.
//

#include "PhysicsSimulator.h"

// TODO: em todos os m�todos verificar se os seus pr�-requisitos s�o atendidos antes de executa-los

PhysicsSimulator PhysicsSimulator::instance = PhysicsSimulator();

PhysicsSimulator::PhysicsSimulator()
{
	isRunning = true;
	runningTime = 0.0f;
	timeStep = 1.0f/60.0f;
}

PhysicsSimulator::PhysicsSimulator(const PhysicsSimulator& physicsSimulator)
{
}

PhysicsSimulator::~PhysicsSimulator()
{
}

PhysicsSimulator &PhysicsSimulator::getInstance()
{
	return instance;
}

bool PhysicsSimulator::initPhysicsSimulation(void* graphicsContext)
{
	mFoundation = PxCreateFoundation(PX_PHYSICS_VERSION, gDefaultAllocatorCallback, gDefaultErrorCallback);
    
    if (!mFoundation)
    {
        // TODO: debug. aqui deve ser criada uma string que seja passada para quem chamou o m�todo resolver o que fazer.
        // o m�todo tamb�m deve retornar um bool que indique se tudo correu como planejado
        cerr << "create foundation failed!" << endl;
    }

	mProfileZoneManager = &PxProfileZoneManager::createProfileZoneManager(mFoundation);

	if (!mProfileZoneManager)
    {
        // TODO: debug. aqui deve ser criada uma string que seja passada para quem chamou o m�todo resolver o que fazer.
        // o m�todo tamb�m deve retornar um bool que indique se tudo correu como planejado
        cerr << "create profile zone manager failed!" << endl;
    }

	PxCudaContextManagerDesc cudaContextManagerDesc;
	cudaContextManagerDesc.interopMode = PxCudaInteropMode::OGL_INTEROP;
	cudaContextManagerDesc.graphicsDevice = graphicsContext;
	mCudaContextManager = PxCreateCudaContextManager(*mFoundation, cudaContextManagerDesc, mProfileZoneManager);
    
	if (mCudaContextManager)
    {
		if(!mCudaContextManager->contextIsValid())
		{
			mCudaContextManager->release();
			mCudaContextManager = NULL;
			cerr << "invalid cuda context manager!" << endl;
		}
    }
	else
	{
        // TODO: debug. aqui deve ser criada uma string que seja passada para quem chamou o m�todo resolver o que fazer.
        // o m�todo tamb�m deve retornar um bool que indique se tudo correu como planejado
        cerr << "create cuda context manager failed!" << endl;
	}

	mPhysics = PxCreatePhysics(PX_PHYSICS_VERSION, *mFoundation, PxTolerancesScale(), false, mProfileZoneManager);
    
    if (!mPhysics)
    {
        // TODO: debug. aqui deve ser criada uma string que seja passada para quem chamou o m�todo resolver o que fazer.
        // o m�todo tamb�m deve retornar um bool que indique se tudo correu como planejado
        cerr << "create physics failed!" << endl;
    }

	createScene(getEarthGravity());
	
    // TODO: criar uma vari�vel string que especifique o erro e possa ser usada por quem chamou o m�todo
	return mPhysics != NULL || !PxInitExtensions(*mPhysics);
}

void PhysicsSimulator::shutdownPhysicsSimulation()
{
	
	mScene->release();
	//TODO: mPhysics->release();
	mProfileZoneManager->release();
	//TODO: mCudaContextManager->release();
	mFoundation->release();

	mActors.clear();
}

PxVec3 PhysicsSimulator::getEarthGravity()
{
	return PxVec3(0.0f, -9.8f, 0.0f);
}

void PhysicsSimulator::stepPhysicsSimulation()
{
    mScene->simulate(timeStep);
    
    // TODO: perform useful work here using previous frame's state data
    
    while(!mScene->fetchResults())
    {
        // TODO: do something useful
    }
}

void PhysicsSimulator::togglePhysicsSimulationRunningState() 
{
	isRunning = !isRunning;
}

PxMaterial *PhysicsSimulator::createMaterial(PxReal staticFriction, PxReal dynamicFriction, PxReal restitution)
{
	return mPhysics->createMaterial(staticFriction, dynamicFriction, restitution);
}

void PhysicsSimulator::addPlane(PxVec3 position, PxQuat orientation, PxMaterial *material)
{
	PxTransform planePosition = PxTransform(position, orientation);
	PxRigidStatic* plane = mPhysics->createRigidStatic(planePosition);
    
    if (!plane)
    {
        // TODO: debug. aqui deve ser criada uma string que seja passada para quem chamou o m�todo resolver o que fazer.
        // o m�todo tamb�m deve retornar um bool que indique se tudo correu como planejado
        cerr << "create plane failed!" << endl;
    }
    
	if(!plane->createShape(PxPlaneGeometry(), *material))
    {
        // TODO: debug. aqui deve ser criada uma string que seja passada para quem chamou o m�todo resolver o que fazer.
        // o m�todo tamb�m deve retornar um bool que indique se tudo correu como planejado
        cerr << "create plane shape failed!" << endl;
    }
    
	mScene->addActor(*plane);
	//TODO: mActors.push_back(plane);
}

RenderActor PhysicsSimulator::addHeightfield(char* heightfieldTexturePath, PxReal thickness, PxReal xScale, PxReal yScale, PxReal zScale, PxMaterial *material)
{
	FREE_IMAGE_FORMAT fif = FIF_UNKNOWN;

	// check the file signature and deduce its format
	fif = FreeImage_GetFileType(heightfieldTexturePath);

	if (fif == FIF_UNKNOWN) // No signature? Try to guess the file format from the file extension
		fif = FreeImage_GetFIFFromFilename(heightfieldTexturePath);

	if ((fif == FIF_UNKNOWN) || !FreeImage_FIFSupportsReading(fif))
	{
		cerr << "Heightfield map texture could not be loaded!" << endl;
		exit(EXIT_FAILURE);
	}

	FIBITMAP *heightfieldTexture = FreeImage_Load(fif, heightfieldTexturePath);

	if(!heightfieldTexture)
	{
		cerr << "Heightfield map texture contains invalid data!" << endl;
		exit(EXIT_FAILURE);
	}

	PxU16 nbColumns = PxU16(FreeImage_GetWidth(heightfieldTexture));
	PxU16 nbRows = PxU16(FreeImage_GetHeight(heightfieldTexture));
	PxHeightFieldDesc heightfieldDesc;
	heightfieldDesc.nbColumns = nbColumns;
	heightfieldDesc.nbRows = nbRows;
	PxU32* samplesData = new PxU32[nbColumns * nbRows];
	heightfieldDesc.samples.data = samplesData;
	heightfieldDesc.samples.stride = sizeof(PxU32);
	heightfieldDesc.convexEdgeThreshold = 0;
	PxU8* currentByte = (PxU8*)heightfieldDesc.samples.data;
	PxU32 texturePitch = PxU32(FreeImage_GetPitch(heightfieldTexture));
	PxU8* loaderPtr = (PxU8*)FreeImage_GetBits(heightfieldTexture);
	RenderActor hfStructure;
	hfStructure.vertices = new vec4[nbColumns * nbRows];
	hfStructure.normals = new vec3[nbColumns * nbRows];
	vec3* adjecencyCount = new vec3[nbColumns * nbRows];
	hfStructure.verticesCount = nbColumns * nbRows;
	hfStructure.uvs = new vec2[nbColumns * nbRows];
	hfStructure.heights = new float[nbColumns * nbRows];

	for(PxU32 row = 0; row < nbRows; row++)
	{
		PxU8* pixel = (PxU8*)loaderPtr;
		for(PxU32 column = 0; column < nbColumns; column++)
		{
			PxHeightFieldSample* currentSample = (PxHeightFieldSample*)currentByte;
			currentSample->height = pixel[FI_RGBA_BLUE];
			hfStructure.heights[row * nbColumns + column] = (float)pixel[FI_RGBA_BLUE];
			hfStructure.vertices[row * nbColumns + column] = vec4(row * xScale, currentSample->height * zScale, column * yScale, 1.0f);
			hfStructure.normals[row * nbColumns + column] = vec3(0.0); // initializing
			adjecencyCount[row * nbColumns + column] = vec3(0.0); // initializing
			hfStructure.uvs[row * nbColumns + column] = vec2((PxReal(row) / PxReal(nbRows)) * 7.0f, (PxReal(column) / PxReal(nbColumns)) * 7.0f);
			currentSample->materialIndex0 = 0;
			currentSample->materialIndex1 = 0;
			currentSample->clearTessFlag();
			currentByte += heightfieldDesc.samples.stride;
			pixel += 3;
		}
		loaderPtr += texturePitch;
	}
	
	// free alocated memory for heightfield texture
	FreeImage_Unload(heightfieldTexture);

	PxHeightField* heightfield = mPhysics->createHeightField(heightfieldDesc);
	// free alocated memory for heightfield samples description
	//free(samplesData);
	// create shape for heightfield
	PxTransform pose(PxVec3(-((PxReal)nbRows * yScale) / 2.0f, 0.0f, -((PxReal)nbColumns * xScale) / 2.0f), PxQuat(PxIdentity));
	PxRigidActor* hf = mPhysics->createRigidStatic(pose);
	PxShape* shape = hf->createShape(PxHeightFieldGeometry(heightfield, PxMeshGeometryFlags(), zScale, xScale, yScale), *material);
	shape->setFlag(PxShapeFlag::ePARTICLE_DRAIN, false);
	shape->setFlag(PxShapeFlag::eSIMULATION_SHAPE, true);
	// add actor to the scene
	mScene->addActor(*hf);
	// create indices and normals
	hfStructure.indices = new uint32[(nbColumns - 1) * (nbRows - 1) * 3 * 2];
	hfStructure.indicesCount = (nbColumns - 1) * (nbRows - 1) * 3 * 2;

	for(int i = 0; i < (nbColumns - 1); ++i)
	{
		for(int j = 0; j < (nbRows - 1); ++j)
		{
			int t1v1 = (i + 1) * nbRows + j;
			int t1v2 = i * nbRows + j;
			int t1v3 = i * nbRows + j + 1;
			int t2v1 = (i + 1) * nbRows + j + 1;
			int t2v2 = (i + 1) * nbRows + j;
			int t2v3 = i * nbRows + j + 1;

			// first triangle
			hfStructure.indices[6 * (i * (nbRows - 1) + j) + 0] = (i + 1) * nbRows + j;
			hfStructure.indices[6 * (i * (nbRows - 1) + j) + 1] = i * nbRows + j;
			hfStructure.indices[6 * (i * (nbRows - 1) + j) + 2] = i * nbRows + j + 1;

			hfStructure.normals[(i + 1) * nbRows + j] += normalize(cross(vec3(hfStructure.vertices[i * nbRows + j] - hfStructure.vertices[(i + 1) * nbRows + j]), vec3(hfStructure.vertices[i * nbRows + j + 1] - hfStructure.vertices[(i + 1) * nbRows + j])));
			hfStructure.normals[i * nbRows + j] += normalize(cross(vec3(hfStructure.vertices[(i + 1) * nbRows + j] - hfStructure.vertices[i * nbRows + j]), vec3(hfStructure.vertices[i * nbRows + j + 1] - hfStructure.vertices[i * nbRows + j])));
			hfStructure.normals[i * nbRows + j + 1] += normalize(cross(vec3(hfStructure.vertices[(i + 1) * nbRows + j] - hfStructure.vertices[i * nbRows + j + 1]), vec3(hfStructure.vertices[i * nbRows + j] - hfStructure.vertices[i * nbRows + j + 1])));

			adjecencyCount[(i + 1) * nbRows + j] += vec3(1.0);
			adjecencyCount[i * nbRows + j] += vec3(1.0);
			adjecencyCount[i * nbRows + j + 1] += vec3(1.0);

			// second triangle
			hfStructure.indices[6 * (i * (nbRows - 1) + j) + 3] = (i + 1) * nbRows + j + 1;
			hfStructure.indices[6 * (i * (nbRows - 1) + j) + 4] = (i + 1) * nbRows + j;
			hfStructure.indices[6 * (i * (nbRows - 1) + j) + 5] = i * nbRows + j + 1;

			hfStructure.normals[(i + 1) * nbRows + j + 1] += normalize(cross(vec3(hfStructure.vertices[(i + 1) * nbRows + j] - hfStructure.vertices[(i + 1) * nbRows + j + 1]), vec3(hfStructure.vertices[i * nbRows + j + 1] - hfStructure.vertices[(i + 1) * nbRows + j + 1])));
			hfStructure.normals[(i + 1) * nbRows + j] += normalize(cross(vec3(hfStructure.vertices[(i + 1) * nbRows + j + 1] - hfStructure.vertices[(i + 1) * nbRows + j]), vec3(hfStructure.vertices[i * nbRows + j + 1] - hfStructure.vertices[(i + 1) * nbRows + j])));
			hfStructure.normals[i * nbRows + j + 1] += normalize(cross(vec3(hfStructure.vertices[(i + 1) * nbRows + j + 1] - hfStructure.vertices[i * nbRows + j + 1]), vec3(hfStructure.vertices[(i + 1) * nbRows + j] - hfStructure.vertices[i * nbRows + j + 1])));

			adjecencyCount[(i + 1) * nbRows + j + 1] += vec3(1.0);
			adjecencyCount[(i + 1) * nbRows + j] += vec3(1.0);
			adjecencyCount[i * nbRows + j + 1] += vec3(1.0);
		}
	}

	// create normals
	for(int i = 0; i < hfStructure.verticesCount; ++i)
	{
		hfStructure.normals[i] /= adjecencyCount[i];
	}

	ShaderReader *shaderReader;
	hfStructure.programs = new GLuint[1];
	hfStructure.programs[0] = shaderReader->getInstance().LoadShader("..\\WaterFlow\\shaders\\heighfield.vert", "..\\WaterFlow\\shaders\\heighfield.frag");
	
	hfStructure.renderActor = hf;

	glGenVertexArrays(1, &hfStructure.VAO);
	glGenBuffers(1, &hfStructure.VBO);
	glGenBuffers(1, &hfStructure.EBO);

	glBindVertexArray(hfStructure.VAO);
		glBindBuffer(GL_ARRAY_BUFFER, hfStructure.VBO);
		glBufferData(GL_ARRAY_BUFFER, hfStructure.verticesCount * sizeof(vec4) + hfStructure.verticesCount * sizeof(vec3) + hfStructure.verticesCount * sizeof(vec2) + hfStructure.verticesCount * sizeof(float), NULL, GL_STATIC_DRAW );
		glBufferSubData(GL_ARRAY_BUFFER, 0, hfStructure.verticesCount * sizeof(vec4), hfStructure.vertices);
		glBufferSubData(GL_ARRAY_BUFFER, hfStructure.verticesCount * sizeof(vec4), hfStructure.verticesCount * sizeof(vec3), hfStructure.normals);
		glBufferSubData(GL_ARRAY_BUFFER, hfStructure.verticesCount * sizeof(vec4) + hfStructure.verticesCount * sizeof(vec3), hfStructure.verticesCount * sizeof(vec2), hfStructure.uvs);
		glBufferSubData(GL_ARRAY_BUFFER, hfStructure.verticesCount * sizeof(vec4) + hfStructure.verticesCount * sizeof(vec3) + hfStructure.verticesCount * sizeof(vec2), hfStructure.verticesCount * sizeof(float), hfStructure.heights);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, hfStructure.EBO);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, hfStructure.indicesCount * sizeof(uint32), hfStructure.indices, GL_STATIC_DRAW);
		glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0) );
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(hfStructure.verticesCount * sizeof(vec4)));
		glEnableVertexAttribArray(1);
		glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(hfStructure.verticesCount * sizeof(vec4) + hfStructure.verticesCount * sizeof(vec3)) );
		glEnableVertexAttribArray(2);
		glVertexAttribPointer(3, 1, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(hfStructure.verticesCount * sizeof(vec4) + hfStructure.verticesCount * sizeof(vec3) + hfStructure.verticesCount * sizeof(vec2)) );
		glEnableVertexAttribArray(3);
	glBindVertexArray(0);
	
	mActors.push_back(hfStructure);

	return hfStructure;
}

ParticleFluidEmitter* PhysicsSimulator::addParticleFluidEmitter(PxU32 maxParticles, PxVec3 position, float releaseDelay, PxReal restitution, PxReal viscosity, PxReal stiffness, PxReal dynamicFriction, PxReal particleDistance)
{
	PxParticleFluid* fluid = mPhysics->createParticleFluid(maxParticles);
	fluid->setGridSize(4.0f);
	fluid->setMaxMotionDistance(0.3f);
	fluid->setRestOffset(particleDistance * 0.5f);
	fluid->setContactOffset(0.36f);
	fluid->setDamping(0.0001f);
	fluid->setRestitution(restitution);
	fluid->setDynamicFriction(dynamicFriction);
	fluid->setRestParticleDistance(particleDistance);
	fluid->setViscosity(viscosity);
	fluid->setStiffness(stiffness);
	fluid->setParticleMass(0.01f);
	fluid->setParticleReadDataFlag(PxParticleReadDataFlag::ePOSITION_BUFFER, true);
	fluid->setParticleReadDataFlag(PxParticleReadDataFlag::eDENSITY_BUFFER, true);
	fluid->setParticleReadDataFlag(PxParticleReadDataFlag::eVELOCITY_BUFFER, true);
	fluid->setParticleReadDataFlag(PxParticleReadDataFlag::eFLAGS_BUFFER, true);
	fluid->setParticleBaseFlag(PxParticleBaseFlag::eCOLLISION_TWOWAY, false);
	//fluid->setParticleBaseFlag(PxParticleBaseFlag::eCOLLISION_TWOWAY, true);
#if PX_SUPPORT_GPU_PHYSX
	fluid->setParticleBaseFlag(PxParticleBaseFlag::eGPU, true);
#endif

	if (!fluid)
	{
		cerr << "create fluid failed!" << endl;
	}

	mScene->addActor(*fluid);
	//mActors.push_back(fluid);

	ShaderReader *shaderReader;
	GLuint* programs = new GLuint[8];
	programs[SHADER_DEPTH] = shaderReader->getInstance().LoadShader("..\\WaterFlow\\shaders\\particles.vert", "..\\WaterFlow\\shaders\\particleDepth.frag");
	glBindFragDataLocation(programs[SHADER_DEPTH], 0, "particleDepth");
	programs[SHADER_FOAM_DEPTH] = shaderReader->getInstance().LoadShader("..\\WaterFlow\\shaders\\particles.vert", "..\\WaterFlow\\shaders\\foamDepth.frag");
	glBindFragDataLocation(programs[SHADER_FOAM_DEPTH], 0, "particleDepth");
	programs[SHADER_THICKNESS] = shaderReader->getInstance().LoadShader("..\\WaterFlow\\shaders\\particles.vert", "..\\WaterFlow\\shaders\\particleThickness.frag");
	glBindFragDataLocation(programs[SHADER_CARTOON_WATER], 0, "particleThickness");
	programs[SHADER_DEPTH_STENCIL] = shaderReader->getInstance().LoadShader("..\\WaterFlow\\shaders\\screenSizedQuad.vert", "..\\WaterFlow\\shaders\\particleDepthStencil.frag");
	glBindFragDataLocation(programs[SHADER_DEPTH_STENCIL], 0, "particleDepthStencil");
	programs[SHADER_EDGES] = shaderReader->getInstance().LoadShader("..\\WaterFlow\\shaders\\screenSizedQuad.vert", "..\\WaterFlow\\shaders\\particleEdges.frag");
	glBindFragDataLocation(programs[SHADER_EDGES], 0, "particleEdges");
	programs[SHADER_BILATERAL_FILTER] = shaderReader->getInstance().LoadShader("..\\WaterFlow\\shaders\\screenSizedQuad.vert", "..\\WaterFlow\\shaders\\bilateralFilter.frag");
	glBindFragDataLocation(programs[SHADER_BILATERAL_FILTER], 0, "smoothedParticleDepth");
	programs[SHADER_CURVATURE_FLOW_FILTER] = shaderReader->getInstance().LoadShader("..\\WaterFlow\\shaders\\screenSizedQuad.vert", "..\\WaterFlow\\shaders\\curvatureFlowFilter.frag");
	glBindFragDataLocation(programs[SHADER_CURVATURE_FLOW_FILTER], 0, "smoothedParticleDepth");
	programs[SHADER_CARTOON_WATER] = shaderReader->getInstance().LoadShader("..\\WaterFlow\\shaders\\screenSizedQuad.vert", "..\\WaterFlow\\shaders\\cartoonWater.frag");
	glBindFragDataLocation(programs[SHADER_CARTOON_WATER], 0, "fColor");
	programs[SHADER_SKYBOX] = shaderReader->getInstance().LoadShader("..\\WaterFlow\\shaders\\skybox.vert", "..\\WaterFlow\\shaders\\skybox.frag");
	glBindFragDataLocation(programs[SHADER_SKYBOX], 0, "fColor");
	programs[SHADER_PARTICLE_CLOUD] = shaderReader->getInstance().LoadShader("..\\WaterFlow\\shaders\\particles.vert", "..\\WaterFlow\\shaders\\particleCloud.frag");
	glBindFragDataLocation(programs[SHADER_PARTICLE_CLOUD], 0, "fColor");

	return new ParticleFluidEmitter(maxParticles, position, fluid, releaseDelay, programs);
}

RenderActor PhysicsSimulator::addBox(PxVec3 position, PxBoxGeometry geometry, PxMaterial *material, PxReal mass, PxReal density, PxRigidDynamic **addedBox, PxReal angularDamping, PxVec3 linearVelocity)
{
    PxRigidDynamic *box;
    box = PxCreateDynamic(*mPhysics, PxTransform(position, PxQuat(PxIdentity)), geometry, *material, density);
    box->setAngularDamping(angularDamping);
    box->setLinearVelocity(linearVelocity);
	box->setMass(mass);
    
    if (!box)
    {
        cerr << "create box failed!" << endl;
    }
    
    mScene->addActor(*box);

	RenderActor boxStructure;

	// The 8 vertexes that makes the box
	boxStructure.vertices = new vec4[8];
	boxStructure.verticesCount = 8;
	boxStructure.vertices[0] = vec4(-geometry.halfExtents.x, -geometry.halfExtents.y,  geometry.halfExtents.z, 1.0f); // left  bottom front
	boxStructure.vertices[1] = vec4(-geometry.halfExtents.x,  geometry.halfExtents.y,  geometry.halfExtents.z, 1.0f); // left  top    front
	boxStructure.vertices[2] = vec4( geometry.halfExtents.x,  geometry.halfExtents.y,  geometry.halfExtents.z, 1.0f); // right top    front
	boxStructure.vertices[3] = vec4( geometry.halfExtents.x, -geometry.halfExtents.y,  geometry.halfExtents.z, 1.0f); // right bottom front
	boxStructure.vertices[4] = vec4(-geometry.halfExtents.x, -geometry.halfExtents.y, -geometry.halfExtents.z, 1.0f); // left  bottom back
	boxStructure.vertices[5] = vec4(-geometry.halfExtents.x,  geometry.halfExtents.y, -geometry.halfExtents.z, 1.0f); // left  top    back
	boxStructure.vertices[6] = vec4( geometry.halfExtents.x,  geometry.halfExtents.y, -geometry.halfExtents.z, 1.0f); // right top    back
	boxStructure.vertices[7] = vec4( geometry.halfExtents.x, -geometry.halfExtents.y, -geometry.halfExtents.z, 1.0f); // right bottom back
							
	// The faces of the box (each composed of two triangles)
	boxStructure.indices = new uint32[36];
	boxStructure.indicesCount = 36;
	boxStructure.indices[0]  = 0; boxStructure.indices[1]  = 3; boxStructure.indices[2]  = 1; boxStructure.indices[3]  = 3; boxStructure.indices[4]  = 1; boxStructure.indices[5]  = 2; // front
	boxStructure.indices[6]  = 3; boxStructure.indices[7]  = 7; boxStructure.indices[8]  = 2; boxStructure.indices[9]  = 7; boxStructure.indices[10] = 2; boxStructure.indices[11] = 6; // right
	boxStructure.indices[12] = 7; boxStructure.indices[13] = 4; boxStructure.indices[14] = 6; boxStructure.indices[15] = 4; boxStructure.indices[16] = 6; boxStructure.indices[17] = 5; // back
	boxStructure.indices[18] = 4; boxStructure.indices[19] = 0; boxStructure.indices[20] = 5; boxStructure.indices[21] = 0; boxStructure.indices[22] = 5; boxStructure.indices[23] = 1; // left
	boxStructure.indices[24] = 4; boxStructure.indices[25] = 7; boxStructure.indices[26] = 0; boxStructure.indices[27] = 7; boxStructure.indices[28] = 0; boxStructure.indices[29] = 3; // bottom
	boxStructure.indices[30] = 1; boxStructure.indices[31] = 2; boxStructure.indices[32] = 5; boxStructure.indices[33] = 2; boxStructure.indices[34] = 5; boxStructure.indices[35] = 6; // top

	// The normals for each of the 8 vertexes of the box
	boxStructure.normals = new vec3[8];
	boxStructure.normals[0] = vec3(-0.577350, -0.577350, -0.577350);
	boxStructure.normals[1] = vec3( 0.816497, -0.408248, -0.408248);
	boxStructure.normals[2] = vec3(-0.408248,  0.816497, -0.408248);
	boxStructure.normals[3] = vec3( 0.408248,  0.408248, -0.816497);
	boxStructure.normals[4] = vec3(-0.408248, -0.408248,  0.816497);
	boxStructure.normals[5] = vec3( 0.408248, -0.816497,  0.408248);
	boxStructure.normals[6] = vec3(-0.816497,  0.408248,  0.408248);
	boxStructure.normals[7] = vec3( 0.577350,  0.577350,  0.577350);

	ShaderReader *shaderReader;
	boxStructure.programs = new GLuint[1];
	boxStructure.programs[0] = shaderReader->getInstance().LoadShader("..\\WaterFlow\\shaders\\box.vert", "..\\WaterFlow\\shaders\\box.frag");

	boxStructure.renderActor = box;

	glGenVertexArrays(1, &boxStructure.VAO);
	glGenBuffers(1, &boxStructure.VBO);
	glGenBuffers(1, &boxStructure.EBO);

	glBindVertexArray(boxStructure.VAO);
		glBindBuffer(GL_ARRAY_BUFFER, boxStructure.VBO);
		glBufferData(GL_ARRAY_BUFFER, boxStructure.verticesCount * sizeof(vec4) + boxStructure.verticesCount * sizeof(vec3), NULL, GL_DYNAMIC_DRAW);
		glBufferSubData(GL_ARRAY_BUFFER, 0, boxStructure.verticesCount * sizeof(vec4), boxStructure.vertices);
		glBufferSubData(GL_ARRAY_BUFFER, boxStructure.verticesCount * sizeof(vec4), boxStructure.verticesCount * sizeof(vec3), boxStructure.normals);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, boxStructure.EBO);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, boxStructure.indicesCount * sizeof(uint32), boxStructure.indices, GL_DYNAMIC_DRAW);
		glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0) );
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(sizeof(boxStructure.vertices)) );
		glEnableVertexAttribArray(1);
	glBindVertexArray(0);

    mActors.push_back(boxStructure);

	*addedBox = box;

	return boxStructure;
}

vector<RenderActor> PhysicsSimulator::getActors()
{
	return mActors;
}

void PhysicsSimulator::createScene(PxVec3 gravity)
{
	// Creating scene
	PxSceneDesc sceneDesc(mPhysics->getTolerancesScale());
	sceneDesc.gravity = gravity;
    
    if(!sceneDesc.cpuDispatcher)
    {
        PxDefaultCpuDispatcher* mCpuDispatcher = PxDefaultCpuDispatcherCreate(1);
        
        if(!mCpuDispatcher)
        {
            // TODO: debug. aqui deve ser criada uma string que seja passada para quem chamou o m�todo resolver o que fazer.
            // o m�todo tamb�m deve retornar um bool que indique se tudo correu como planejado
            cerr << "PxDefaultCpuDispatcherCreate failed!" << endl;
        }
        
        sceneDesc.cpuDispatcher = mCpuDispatcher;
    }

	if(!sceneDesc.gpuDispatcher && mCudaContextManager)
    {
		PxGpuDispatcher* mGpuDispatcher = mCudaContextManager->getGpuDispatcher();
        
        if(!mGpuDispatcher)
        {
            // TODO: debug. aqui deve ser criada uma string que seja passada para quem chamou o m�todo resolver o que fazer.
            // o m�todo tamb�m deve retornar um bool que indique se tudo correu como planejado
            cerr << "PxGpuDispatcher creation failed!" << endl;
        }
        
        sceneDesc.gpuDispatcher = mGpuDispatcher;
    }
    
    if(!sceneDesc.filterShader)
    {
        sceneDesc.filterShader = PxDefaultSimulationFilterShader;
    }
    
	mScene = mPhysics->createScene(sceneDesc);
    
    if (!mScene)
    {
        // TODO: debug. aqui deve ser criada uma string que seja passada para quem chamou o m�todo resolver o que fazer.
        // o m�todo tamb�m deve retornar um bool que indique se tudo correu como planejado
        cerr << "createScene failed!" << endl;
    }
    
    mScene->setVisualizationParameter(PxVisualizationParameter::eSCALE, 1.0);
    mScene->setVisualizationParameter(PxVisualizationParameter::eCOLLISION_SHAPES, 1.0f);
}

