#define BUFFER_OFFSET(bytes) ((GLubyte*) NULL + (bytes))

#include <iostream> // TODO: usado para debug, remover quando tirar os debugs
#include <fstream>
#include <vector>

#include <PhysX\PxPhysicsAPI.h>
#include <PhysX\pxtask\PxTask.h>
#include <GL\glew.h>
#include <glm\glm.hpp>
#include <glm\gtc\matrix_transform.hpp>
#include <glm\gtc\type_ptr.hpp>
#include <glm\gtx\fast_square_root.hpp>
#include <glm\gtc\quaternion.hpp>
#include <FreeImage.h>

#include "RenderActor.h"
#include "ParticleFluidEmitter.h"
#include "ShaderReader.h"

using namespace glm;
using namespace std;
using namespace physx;

class PhysicsSimulator
{
public:
	static PhysicsSimulator &getInstance();

	bool initPhysicsSimulation(void* graphicsContext);
	void shutdownPhysicsSimulation();

	PxVec3 getEarthGravity();
    
	void stepPhysicsSimulation();
	void togglePhysicsSimulationRunningState();
	
	PxMaterial *createMaterial(PxReal staticFriction, PxReal dynamicFriction, PxReal restitution);
	
	void addPlane(PxVec3 position, PxQuat orientation, PxMaterial *material);
	RenderActor addHeightfield(char* heightfieldTexturePath, PxReal thickness, PxReal xScale, PxReal yScale, PxReal zScale, PxMaterial *material);
	ParticleFluidEmitter* PhysicsSimulator::addParticleFluidEmitter(PxU32 maxParticles, PxVec3 position, float releaseDelay, PxReal restitution, PxReal viscosity, PxReal stiffness, PxReal dynamicFriction, PxReal particleDistance);
	RenderActor addBox(PxVec3 position, PxBoxGeometry geometry, PxMaterial *material, PxReal mass, PxReal density, PxRigidDynamic **addedBox, PxReal angularDamping = 0.5, PxVec3 linearVelocity = PxVec3(0.0f, 0, 0.0f));
	vector<RenderActor> getActors();
    
	bool isRunning;
    PxPhysics* mPhysics;

    virtual ~PhysicsSimulator();

private:
	PhysicsSimulator();
	PhysicsSimulator(const PhysicsSimulator& physicsSimulator);

	static PhysicsSimulator instance;

	void createScene(PxVec3 gravity);
    
	PxDefaultErrorCallback	gDefaultErrorCallback;
	PxDefaultAllocator		gDefaultAllocatorCallback;

	PxFoundation*			mFoundation;
	PxProfileZoneManager*	mProfileZoneManager;
	PxCudaContextManager*	mCudaContextManager;
	PxScene*				mScene;
    
	vector<RenderActor> mActors;
    
	PxReal	runningTime;
	PxReal	timeStep;
};
