# Screen Space Cartoon Water Rendering

Method for rendering particle-based liquid simulations that runs in real-time and includes an adjustable performance/quality
trade-off using an iterative version of the separated bilateral filter and foam/droplets.